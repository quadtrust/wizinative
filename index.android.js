import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  AppState
} from 'react-native';
import Loader from './components/Loader';
import Sounds from './components/Sounds';

export default class wizi extends Component {
  constructor(props){
    super(props);

    this.state = {
      appState: AppState.currentState
    }
  }

  handleAppStateChange(nextAppState) {
    if(this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      Sounds.resumeBackConditional();
    }else {
      Sounds.pauseBackground();
    }
    this.setState({appState: nextAppState});
  }

  componentDidMount() {
    Sounds.loadSounds();
    AppState.addEventListener('change', this.handleAppStateChange.bind(this));
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange.bind(this));
  }

  render() {
    return (
      <View style={styles.fullWidth}>
        <Loader />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fullWidth: {
    flex: 1
  }
});

AppRegistry.registerComponent('wizi', () => wizi);
