import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    AsyncStorage,
    TouchableOpacity,
    ListView,
    ToastAndroid
} from 'react-native';
import styles from './Style';
import Header from './Header';
import GridView from 'react-native-easy-gridview';
import API from './API';

export default class UnlockCharacter extends Component {
    constructor(props) {
        super(props);
        let ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.state = {
            avatars: ds.cloneWithRows([]),
            unlocked: false
        }
    }

    componentDidMount() {
        API.getUnlockedAvatars(this.props.doResetTo, (response) => {
            if(response.error) {
                ToastAndroid.show('Network error', ToastAndroid.SHORT);
                this.props.doPop();
            } else {
                let data = Array.apply(null, {length: 50}).map(Number.call, Number);
                this.setState({
                    unlocked: response.avatars,
                    avatars: this.state.avatars.cloneWithRows(data)
                });
            }
        });
    }

    _chooseChar(char) {
        API.unlockAvatar(char, this.props.doResetTo, (response) => {
            if(response.error){
                ToastAndroid.show('Network error', ToastAndroid.SHORT);
            } else {
                AsyncStorage.setItem('character', JSON.stringify(char)).then(() => {
                        this.props.doReplace('characterUnlocked');
                    }
                );
            }
        });
    }

    avatarRow(a) {
        return <View style={{margin: 5}}>
            <TouchableOpacity
                disabled={(this.state.unlocked !== false ? (this.state.unlocked.indexOf(a + 1) >= 0) : false)}
                onPress={() => this._chooseChar(a+1)}
            >
                <Image
                    source={API.map_avatar(a + 1)}
                    style={{
                        flex: 1,
                        height: 100,
                        width: undefined,
                        opacity: (this.state.unlocked !== false ? (this.state.unlocked.indexOf(a+1) > -1 ? 1 : 0.2) : 0.2)
                    }}
                    resizeMode={'contain'}
                />
            </TouchableOpacity>
        </View>
    }

    render() {
        return (
            <View style={styles.fullWidth}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true}/>
                <View style={[styles.bodyWidth, styles.backBlack]}>
                    <Text style={[styles.avatar_header, styles.center]}>UNLOCK AN AVATAR</Text>
                    <GridView
                        dataSource={this.state.avatars}
                        renderRow={(item) => this.avatarRow(item)}
                        numberOfItemsPerRow={4}
                        removeClippedSubviews={false}
                        initialListSize={1}
                        pageSize={5}
                    />
                </View>
            </View>
        );
    }
}