import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    DatePickerAndroid,
    ScrollView,
    TextInput,
    ToastAndroid,
    AsyncStorage,
    TouchableOpacity
} from 'react-native';
import language from './Language';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import Button from 'react-native-button';
import styles from './Style';
import API from './API';
import Picker from 'react-native-picker';
let _dobField;
let radio_props = [
    { label: language.boy, value: 0 },
    { label: language.girl, value: 1 }
];


export default class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            school: '',
            dob: '',
            email: '',
            username: '',
            age: '',
            language: '',
            gender: 1,
            standard: '',
            standardLabel: '',
            password: '',
            password_confirm: '',
            availableStandards: [],
            availableDirectorates: [],
            directorate: '',
            directorateLabel: ''
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('userFb').then((fb) => {
            if (fb != null) {
                fb = JSON.parse(fb);
                this.setState({
                    firstName: fb.first_name,
                    lastName: fb.last_name,
                    email: fb.email
                });
                AsyncStorage.removeItem('userFb');
            }
        });

        AsyncStorage.getItem('userGoogle').then((google) => {
            if (google != null) {
                google = JSON.parse(google);
                let name = google.name.split(" ");
                this.setState({
                    firstName: name[0],
                    lastName: name[1],
                    email: google.email
                });
                AsyncStorage.removeItem('userGoogle');
            }
        });

        API.get_standards(this.props.doResetTo, (response) => {
            this.setState({
                availableStandards: response.classes
            });
        });

        API.getDirectorates((directorates) => {
            this.setState({
                availableDirectorates: directorates.directorates
            });
        });
    }

    showLanguagePicker() {
        Picker.init({
            pickerData: ['ENGLISH', 'ARABIC'],
            selectedValue: [this.state.language],
            onPickerConfirm: data => {
                this.setState({
                    language: data[0]
                });
            },
            pickerTitleText: 'CHOOSE LANGUAGE',
            pickerConfirmBtnText: 'CHOOSE',
            pickerCancelBtnText: 'CANCEL'
        });
        Picker.show();
    }

    showStandardPicker() {
        Picker.init({
            pickerData: this.state.availableStandards.map((a) => {
                return a.class
            }),
            selectedValue: [this.state.standard],
            onPickerConfirm: data => {
                this.state.availableStandards.forEach((a) => {
                    if(a.class == data) {
                        this.setState({
                            standard: a.id,
                            standardLabel: data
                        });
                    }
                });
            },
            pickerTitleText: 'CHOOSE STANDARD',
            pickerConfirmBtnText: 'CHOOSE',
            pickerCancelBtnText: 'CANCEL'
        });
        Picker.show();
    }

    showDirectoratePicker() {
        Picker.init({
            pickerData: this.state.availableDirectorates.map((a) => {
                return a.name
            }),
            selectedValue: [this.state.directorate],
            onPickerConfirm: data => {
                this.state.availableDirectorates.forEach((a) => {
                    if(a.name == data) {
                        this.setState({
                            directorate: a.id,
                            directorateLabel: data
                        });
                    }
                });
            },
            pickerTitleText: 'CHOOSE DIRECTORATE',
            pickerConfirmBtnText: 'CHOOSE',
            pickerCancelBtnText: 'CANCEL'
        });
        Picker.show();
    }

    showPicker = async (options) => {
        try {
            currentDate = new Date();
            const { action, year, month, day } = await DatePickerAndroid.open({ maxDate: new Date(currentDate.getFullYear() - 18, currentDate.getMonth(), currentDate.getDate() - 1) });
            if (action === DatePickerAndroid.dismissedAction) {
                _dobField.blur();
            } else {
                let date = new Date(year, month, day);
                this.focusNextField("7");
                this.setState({
                    dob: year + '/' + (month + 1) + '/' + day
                });
            }
        } catch ({ code, message }) {
            ToastAndroid.show('Error!', ToastAndroid.SHORT)
        }
    };

    _register() {
        if (
            this.state.firstName === '' ||
            this.state.lastName === '' ||
            this.state.school === '' ||
            this.state.dob === '' ||
            this.state.email === '' ||
            this.state.username === '' ||
            this.state.age === '' ||
            this.state.language === '' ||
            this.state.standard === '' ||
            this.state.password === '' ||
            this.state.password_confirm === '' ||
            this.state.directorate === ''
        ) {
            ToastAndroid.show('Kindly fill all the fields.', ToastAndroid.SHORT)
        } else if (this.state.password !== this.state.password_confirm) {
            ToastAndroid.show('Passwords do not match', ToastAndroid.SHORT)
        } else {
            this.props.loading();
            API.register(this.state.firstName, this.state.lastName, this.state.school, this.state.dob, this.state.email, this.state.username, this.state.age, this.state.language, this.state.gender, this.state.standard, this.state.password, this.state.password_confirm, this.state.directorate, (resp) => {
                this.props.loaded();
                if (resp.error) {
                    ToastAndroid.show(resp.message, ToastAndroid.SHORT)
                } else {
                    AsyncStorage.setItem('user', JSON.stringify(resp)).then(() => {
                        ToastAndroid.show('You have successfully registered', ToastAndroid.SHORT);
                        this.props.doLoad('newGame');
                    });
                }
            });
        }

    }

    _gender(value) {
        this.setState({ gender: value });
    }

    focusNextField = (nextField) => {
        this.refs[nextField].focus();
    };

    render() {
        return (
            <View style={styles.fullWidth}>
                <Image
                    style={[styles.fullWidth, styles.zoomed, styles.notKnown]}
                    source={require('../assets/images/register/back.png')}
                >
                    <Text
                        style={[{ textAlign: 'center', fontSize: 28, fontWeight: '800', paddingTop: 20 }, styles.white]}>
                        {language.registerBelow}
                    </Text>
                </Image>
                <View style={{ flex: 3, justifyContent: 'space-between', marginTop: 10 }}>
                    <ScrollView>
                        <View>

                            <TextInput
                                ref="1"
                                style={[styles.field, styles.black, styles.margins, { fontWeight: '800', marginTop: 20 }]}
                                placeholder={language.firstName}
                                placeholderTextColor="grey"
                                underlineColorAndroid="transparent"
                                keyboardType="default"
                                onChangeText={firstName => this.setState({ firstName })}
                                value={this.state.firstName}
                                onSubmitEditing={() => this.focusNextField('2')}
                            />
                            <TextInput
                                ref="2"
                                style={[styles.field, styles.black, styles.margins, { fontWeight: '800', marginTop: 10 }]}
                                placeholder={language.lastName}
                                placeholderTextColor="grey"
                                underlineColorAndroid="transparent"
                                onChangeText={lastName => this.setState({ lastName })}
                                value={this.state.lastName}
                                onSubmitEditing={() => this.focusNextField('3')}
                            />
                            <TextInput
                                ref="3"
                                style={[styles.field, styles.black, styles.margins, { fontWeight: '800', marginTop: 10 }]}
                                placeholder={language.username}
                                placeholderTextColor="grey"
                                underlineColorAndroid="transparent"
                                onChangeText={username => this.setState({ username })}
                                value={this.state.username}
                            />

                            <TouchableOpacity style={[styles.field, styles.margins, { marginTop: 10, justifyContent: 'center' }]} onPress={this.showLanguagePicker.bind(this, {})}>
                                {
                                    this.state.language === '' ?
                                        <Text style={{fontWeight: '800', color: '#808080'}}>CHOOSE LANGUAGE</Text>
                                    :
                                        <Text style={{fontWeight: '800', color: 'black'}}>{this.state.language}</Text>
                                }
                            </TouchableOpacity>

                            <TextInput
                                ref="5"
                                style={[styles.field, styles.black, styles.margins, { fontWeight: '800', marginTop: 10 }]}
                                placeholder={language.age}
                                placeholderTextColor="grey"
                                keyboardType="phone-pad"
                                underlineColorAndroid="transparent"
                                onChangeText={age => this.setState({ age })}
                                value={this.state.age}
                                onSubmitEditing={() => _dobField.focus()}
                            />
                            <TextInput
                                ref={(dob) => {
                                    _dobField = dob;
                                }}
                                style={[styles.field, styles.black, styles.margins, { fontWeight: '800', marginTop: 10 }]}
                                placeholder={language.dateOfBirth}
                                placeholderTextColor="grey"
                                underlineColorAndroid="transparent"
                                onChangeText={dob => this.setState({ dob })}
                                value={this.state.dob}
                                onFocus={this.showPicker.bind(this, {})}
                            />

                            <TextInput
                                ref="7"
                                style={[styles.field, styles.black, styles.margins, { fontWeight: '800', marginTop: 10 }]}
                                placeholder={language.nameSchool}
                                placeholderTextColor="grey"
                                underlineColorAndroid="transparent"
                                onChangeText={school => this.setState({ school })}
                                value={this.state.school}
                                returnKeyType="next"
                            />

                            <TouchableOpacity style={[styles.field, styles.margins, { marginTop: 10, justifyContent: 'center' }]} onPress={this.showStandardPicker.bind(this, {})}>
                                {
                                    this.state.standard === '' ?
                                        <Text style={{fontWeight: '800', color: '#808080'}}>CHOOSE STANDARD</Text>
                                        :
                                        <Text style={{fontWeight: '800', color: 'black'}}>{this.state.standardLabel}</Text>
                                }
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.field, styles.margins, { marginTop: 10, justifyContent: 'center' }]} onPress={this.showDirectoratePicker.bind(this, {})}>
                                {
                                    this.state.directorate === '' ?
                                        <Text style={{fontWeight: '800', color: '#808080'}}>CHOOSE DIRECTORATE</Text>
                                        :
                                        <Text style={{fontWeight: '800', color: 'black'}}>{this.state.directorateLabel}</Text>
                                }
                            </TouchableOpacity>

                            {/*<Picker
                                selectedValue={this.state.standard}
                                mode='dropdown'
                                style={[styles.field, styles.black, styles.margins, { marginTop: 10, color: 'grey' }]}
                                onValueChange={(standard) => this.setState({ standard })}>

                                {this.state.availableStandards.map((standard, index) => {
                                    return <Picker.Item key={standard.id} label={standard.class} value={standard.class} />;
                                })}

                            </Picker>

                            <Picker
                                selectedValue={this.state.standard}
                                mode='dropdown'
                                style={[styles.field, styles.black, styles.margins, { marginTop: 10, color: 'grey' }]}
                                onValueChange={(directorate) => this.setState({ directorate })}>

                                {this.state.availableDirectorates.map((directorate, index) => {
                                    return <Picker.Item key={directorate.id} label={directorate.name} value={directorate.id} />;
                                })}

                            </Picker>*/}

                            <TextInput
                                ref="10"
                                style={[styles.field, styles.black, styles.margins, { fontWeight: '800', marginTop: 10 }]}
                                placeholder={language.email}
                                keyboardType="email-address"
                                placeholderTextColor="grey"
                                underlineColorAndroid="transparent"
                                onChangeText={email => this.setState({ email })}
                                value={this.state.email}
                                returnKeyType="next"
                                onSubmitEditing={() => this.focusNextField('11')}
                            />
                            <TextInput
                                ref="11"
                                style={[styles.field, styles.black, styles.margins, { fontWeight: '800', marginTop: 10 }]}
                                placeholder={language.password}
                                placeholderTextColor="grey"
                                underlineColorAndroid="transparent"
                                secureTextEntry={true}
                                onChangeText={password => this.setState({ password })}
                                value={this.state.password}
                                returnKeyType="next"
                                onSubmitEditing={() => this.focusNextField('12')}
                            />
                            <TextInput
                                ref="12"
                                style={[styles.field, styles.black, styles.margins, { fontWeight: '800', marginTop: 10 }]}
                                placeholder={language.confirmPassword}
                                placeholderTextColor="grey"
                                secureTextEntry={true}
                                underlineColorAndroid="transparent"
                                onChangeText={password_confirm => this.setState({ password_confirm })}
                                value={this.state.password_confirm}
                                returnKeyType="done"
                            />
                        </View>
                        <View>
                            <RadioForm style={[styles.margins, {
                                marginTop: 10,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }]}
                                radio_props={radio_props}
                                initial={0}
                                formHorizontal={true}
                                labelHorizontal={true}
                                buttonColor={'#F44E4B'}
                                animation={true}
                                onPress={(value) => {
                                    this.setState({ value: value });
                                    this._gender(value);
                                }}
                                labelStyle={{ padding: 10 }}
                            />
                        </View>
                        <View>
                            <Button
                                containerStyle={[styles.socialButton, styles.red, {
                                    marginTop: 20,
                                    marginLeft: 120,
                                    marginRight: 120
                                }]}
                                style={[{ fontSize: 18, color: 'white' }]}
                                onPress={() => this._register()}
                            >REGISTER</Button>
                        </View>

                    </ScrollView>
                </View>

            </View>
        );
    }
}

