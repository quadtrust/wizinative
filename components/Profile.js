import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  AsyncStorage,
  Image,
  TextInput,
  ToastAndroid
} from 'react-native'
import Header from './Header'
import Button from 'react-native-button'
import API from './API'
import moment from 'moment'

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      welcome: '',
      dob: '',
      country: '',
      number: '',
      school: '',
      email: '',
      editable: false,
      mobile: '',
      image: require('../assets/images/user.png'),
      questions_attempted: 0,
      tests_attempted: 0,
      overall_points: 0
    }
  }

  componentDidMount() {
    API.get_me(this.props.doResetTo, (resp) => {
      this.setState({
        username: resp.user.username.toUpperCase(),
        welcome: resp.user.username.toUpperCase(),
        email: resp.user.email,
        number: resp.user.mobile,
        school: resp.user.school,
        dob: moment(resp.user.dob).format('MMMM Do YYYY'),
        country: resp.user.country,
        image: API.map_avatar(resp.user.image)
      });
    });
    let a = this;
    API.getResult(this.props.doResetTo, (result) => {
      console.log(result);
      let questions_asked = 0, questions_answered = 0, overall_points = 0;
      result.result.forEach(function (r) {
        questions_answered += r.questions_answered;
        questions_asked += r.questions_asked / 10;
        overall_points += r.questions_answered_correctly;
        a.setState({
          questions_attempted: questions_answered,
          tests_attempted: questions_asked,
          overall_points: overall_points
        });
      });
    });
  }

  editOrSaveProfile() {
    if (this.state.editable) {
      this.setState({
        editable: false
      });
      API.update_user({
        school: this.state.school,
        mobile: this.state.mobile,
        country: this.state.country
      }, this.props.doResetTo, (response) => {
        if (response.error) {
          ToastAndroid.show('There was some error, please try again', ToastAndroid.SHORT);
        } else {
          ToastAndroid.show('Profile updated', ToastAndroid.SHORT);
        }
      });
    } else {
      this.setState({
        editable: true
      });
    }
  }

  render() {
    let usernameField, dobField, countryField, schoolField, buttonText, mobileNumber, labelPadding;
    if (this.state.editable) {
      usernameField = <TextInput ref='first_field' style={{ color: 'white', height: 23, padding: 0, margin: 0 }} value={this.state.username} onChangeText={(username) => this.setState({ username })} />;
      dobField = <TextInput style={{ color: 'white', height: 23, padding: 0, margin: 0 }} value={this.state.dob} onChangeText={(dob) => this.setState({ dob })} />;
      countryField = <TextInput style={{ color: 'white', height: 23, padding: 0, margin: 0 }} value={this.state.country} onChangeText={(country) => this.setState({ country })} />;
      schoolField = <TextInput style={{ color: 'white', height: 23, padding: 0, margin: 0 }} value={this.state.school} onChangeText={(school) => this.setState({ school })} />;
      mobileNumber = <TextInput style={{ color: 'white', height: 23, padding: 0, margin: 0 }} value={this.state.number} onChangeText={(number) => this.setState({ number })} />;
      buttonText = "SAVE PROFILE";
    } else {
      usernameField = <Text style={[styles.white, styles.bold, styles.mediumFont]}> {this.state.username}</Text>;
      dobField = <Text style={[styles.white, styles.bold, styles.mediumFont]}> {this.state.dob}</Text>;
      countryField = <Text style={[styles.white, styles.bold, styles.mediumFont]}> {this.state.country}</Text>;
      schoolField = <Text style={[styles.white, styles.bold, styles.mediumFont]}> {this.state.school}</Text>;
      mobileNumber = <Text style={[styles.white, styles.bold, styles.mediumFont]}> {this.state.number}</Text>;
      buttonText = "EDIT PROFILE";
    }

    return (
      <View style={styles.head}>
          <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true} />
          <View style={[styles.body]}>
              <ScrollView>
                  <Text
                    style={[styles.h1, styles.padVertical, styles.white, styles.textCenter, styles.bold, styles.backGreen]}>PROFILE</Text>
                  <Text
                    style={[styles.textCenter, styles.white, styles.padVertical, styles.bold, styles.h1, styles.h2, styles.backBlue]}>
                      WELCOME {this.state.welcome}</Text>
                  <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                      <View style={[styles.row]}>
                          <View >
                              <Image source={this.state.image} resizeMode={"contain"} style={{ height: 100, width: 100 }} />
                          </View>
                          <View style={[styles.center]}>
                              <Button
                                containerStyle={[styles.button1, { marginLeft: 20 }]}
                                style={[styles.textCenter, styles.bold, { color: 'white', marginTop: 2 }]}
                                onPress={() => {
                                            this.editOrSaveProfile()
                                        }}
                              >
                                {buttonText}
                              </Button>
                              <Button
                                containerStyle={[styles.button2, { marginLeft: 20 }]}
                                style={[styles.textCenter, { color: 'white', marginTop: 2 }]}>
                                  EDIT PASSWORD
                              </Button>
                          </View>

                      </View>

                      <View style={[{ marginTop: 20 }, styles.row]}>
                          <View style={[styles.padVertical, { flex: 1, justifyContent: 'center' }]}>
                              <Text style={[styles.white, styles.bold, styles.mediumFont]}> USERNAME:</Text>
                              <Text style={[styles.white, styles.bold, styles.mediumFont]}> DOB:</Text>
                              <Text style={[styles.white, styles.bold, styles.mediumFont]}> COUNTRY:</Text>
                              <Text style={[styles.white, styles.bold, styles.mediumFont]}> MOBILE:</Text>
                              <Text style={[styles.white, styles.bold, styles.mediumFont]}> SCHOOL:</Text>
                              <Text style={[styles.white, styles.bold, styles.mediumFont]}> EMAIL:</Text>
                          </View>
                          <View style={[styles.padVertical, { flex: 2 }]}>
                            {usernameField}
                            {dobField}
                            {countryField}
                            {mobileNumber}
                            {schoolField}
                              <Text style={[styles.white, styles.bold, styles.mediumFont]}> {this.state.email}</Text>
                          </View>
                      </View>


                  </View>
                  <View style={[styles.backFb, styles.center, { marginTop: 10 }]}>
                      <Text style={[styles.white, styles.bold, styles.h1, styles.padVertical]}>
                          SUMMARY
                      </Text>
                  </View>
                  <View style={[styles.row]}>
                      <View style={[styles.center, { flex: 2, marginTop: 5, borderLeftWidth: 2, borderColor: '#424041' }]}>
                          <Text style={[styles.white, styles.bold, styles.textCenter, styles.mediumFont]}>
                              QUESTION ATTEMPED
                          </Text>
                          <Text style={[styles.yellow, styles.bold, styles.h1]}>
                            {this.state.questions_attempted}
                          </Text>
                      </View>
                      <View style={[styles.center, { flex: 2, marginTop: 5, borderLeftWidth: 2, borderColor: '#424041' }]}>
                          <Text style={[styles.white, styles.bold, styles.textCenter, styles.mediumFont]}>
                              TESTS ATTEMPED
                          </Text>
                          <Text style={[styles.yellow, styles.bold, styles.h1]}>
                            {this.state.tests_attempted}
                          </Text>
                      </View>
                      <View style={[styles.center, { flex: 2, marginTop: 5, borderLeftWidth: 2, borderColor: '#424041' }]}>
                          <Text style={[styles.white, styles.bold, styles.textCenter, styles.mediumFont]}>
                              OVERALL POINTS
                          </Text>
                          <Text style={[styles.yellow, styles.bold, styles.h1]}>
                            {this.state.overall_points}
                          </Text>
                      </View>
                  </View>
              </ScrollView>
          </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  head: {
    flex: 1,
    backgroundColor: '#2B292A'
  },
  button1: {
    backgroundColor: '#ED145B', width: 150, height: 30,
  },
  button2: {
    backgroundColor: '#ED145B', width: 150, height: 30, marginTop: 20,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  row: {
    flexDirection: 'row'
  },

  body: {
    flex: 18
  },
  backGreen: {
    backgroundColor: '#3DBAA6'
  },
  backBlue: { backgroundColor: '#3FC1FF' },
  backFb: { backgroundColor: '#4276BA' },
  yellow: { color: '#FCFE00' },
  white: {
    color: 'white'
  },
  padVertical: {
    paddingTop: 5,
    paddingBottom: 5
  },
  h1: {
    fontSize: 30
  },
  mediumFont: {
    fontSize: 17
  },
  h2: {
    fontSize: 20
  },
  textCenter: {
    textAlign: 'center'
  },
  bold: {
    fontWeight: '800'
  },
  black: {
    color: '#000'
  },

  margins: {
    marginLeft: 50,
    marginRight: 50
  },
});

