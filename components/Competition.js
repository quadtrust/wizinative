import React, {Component} from 'react'
import {
    View,
    Text,
    Image
} from 'react-native'
import styles from './Style'
import Header from './Header'

export default class Competition extends Component {
    constructor(props){
        super(props);
        this.state ={
            char1:'Bhavya',
            char2:'Neeraj',
            char3:'Sankalp',
            char4:'Tousif',
            char5:'Teja',
            char6:'Prithvi'
        }
    }
    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop}/>
                <View style={[styles.bodyWidth, styles.whiteBack,{alignItems:'center'}]}>
                    <Text style={[styles.center, styles.competitionText]}>{'Competition'.toUpperCase()}</Text>
                    <Image source={require('../assets/images/map.png')} style={{height:250,width:320, resizeMode:'contain'}}/>
                    <View style={[styles.row,{justifyContent:'space-around',marginLeft:10,marginRight:10,borderBottomWidth:1,borderColor:'black'}]}>
                        <View style={{flex:1}}>
                            <Text style={{color:'#AA0922', fontSize:15,fontWeight:'700'}}>GOVERNORATE</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#AA0922', fontSize:15,textAlign:'center',fontWeight:'700'}}>PLAYERS</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#AA0922', fontSize:15,textAlign:'right',fontWeight:'700'}}>SCORES</Text>
                        </View>

                    </View>
                    <View style={[styles.row,{justifyContent:'space-around',marginLeft:10,marginRight:10,borderBottomWidth:1,borderColor:'black'}]}>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,fontWeight:'700'}}>{this.state.char5}</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'center',fontWeight:'700'}}>2</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'right',fontWeight:'700'}}>1200</Text>
                        </View>

                    </View>
                    <View style={[styles.row,{justifyContent:'space-around',marginLeft:10,marginRight:10,borderBottomWidth:1,borderColor:'black'}]}>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,fontWeight:'700'}}>{this.state.char1}</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'center',fontWeight:'700'}}>2</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'right',fontWeight:'700'}}>1200</Text>
                        </View>
                    </View>
                    <View style={[styles.row,{justifyContent:'space-around',marginLeft:10,marginRight:10,borderBottomWidth:1,borderColor:'black'}]}>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,fontWeight:'700'}}>{this.state.char2}</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'center',fontWeight:'700'}}>2</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'right',fontWeight:'700'}}>1200</Text>
                        </View>
                    </View>
                    <View style={[styles.row,{justifyContent:'space-around',marginLeft:10,marginRight:10,borderBottomWidth:1,borderColor:'black'}]}>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,fontWeight:'700'}}>{this.state.char3}</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'center',fontWeight:'700'}}>2</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'right',fontWeight:'700'}}>1200</Text>
                        </View>
                    </View>
                    <View style={[styles.row,{justifyContent:'space-around',marginLeft:10,marginRight:10,borderBottomWidth:1,borderColor:'black'}]}>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,fontWeight:'700'}}>{this.state.char4}</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'center',fontWeight:'700'}}>2</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:'#159CFF', fontSize:15,textAlign:'right',fontWeight:'700'}}>1200</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}