import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    ToastAndroid,
    AsyncStorage,
    Animated
} from 'react-native';
import language from './Language';
import Header from './Header';
import PlayerStats from './PlayerStats';
import Sounds from './Sounds';

TIMES = 400;
let rotation;
let last_spin = 0;
export default class Spinner extends Component {
    constructor(props) {
        super(props);

        this.state = {
            subjectRotation: new Animated.Value(0),
            spinning: false
        }
    }

    _spin() {
        if(!this.state.spinning) {
            this.setState({spinning: true});

            let subjects_rotate = Math.floor(Math.random() * 360) + 1;
            if (
                (subjects_rotate >= 31 && subjects_rotate <= 36) ||
                (subjects_rotate >= 91 && subjects_rotate <= 96) ||
                (subjects_rotate >= 149 && subjects_rotate <= 155) ||
                (subjects_rotate >= 210 && subjects_rotate <= 216) ||
                (subjects_rotate >= 269 && subjects_rotate <= 275) ||
                (subjects_rotate >= 329 && subjects_rotate <= 335)
            ) {
                subjects_rotate += 6
            }
            subjects_rotate = subjects_rotate + (3 * 360);
            subjects_rotate += last_spin;
            last_spin = subjects_rotate;
            if (this.props.spins == 0) {
                ToastAndroid.show('Sorry you cannot spin more', ToastAndroid.SHORT);
            } else {
                Sounds.playSpinner();
                this.props.reduceSpin();
                rotation = '';
                this.state.subjectRotation.addListener((value) => {
                    rotation = JSON.stringify(value.value) % 360;
                });
                // this.state.subjectRotation.setValue(0);
                Animated.timing(                          // Base: spring, decay, timing
                    this.state.subjectRotation,
                    {
                        toValue: subjects_rotate,
                        // toValue: 40,
                        // friction:50,
                        duration: 3000
                    }
                ).start((resp) => {
                    if (resp.finished) {
                        this.setState({spinning: false});
                        if (rotation > 36 && rotation <= 91) {
                            AsyncStorage.setItem('subject', 'MATHS').then(() => {
                                this.props.doLoad('selected');
                            });

                        } else if (rotation > 96 && rotation <= 149) {
                            AsyncStorage.setItem('subject', 'SOCIAL STUDIES').then(() => {
                                this.props.doLoad('selected');
                            });
                        } else if (rotation > 155 && rotation <= 210) {
                            AsyncStorage.setItem('subject', 'ISLAMIC STUDIES').then(() => {
                                this.props.doLoad('selected');
                            });
                        } else if (rotation > 216 && rotation <= 269) {
                            AsyncStorage.setItem('subject', 'SCIENCE').then(() => {
                                this.props.doLoad('selected');
                            });
                        } else if (rotation > 275 && rotation <= 329) {
                            AsyncStorage.setItem('subject', 'ENGLISH').then(() => {
                                this.props.doLoad('selected');
                            });
                        } else {
                            AsyncStorage.setItem('subject', 'ARABIC').then(() => {
                                this.props.doLoad('selected');
                            });
                        }
                    }
                });
            }
        }
    }

    componentDidMount() {


    }
    render() {
        const spin = this.state.subjectRotation.interpolate({
            inputRange: [0, 360],
            outputRange: ['0deg', '360deg']
        });
        return (
            <View style={styles.fullWidth}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} player={true} />
                <PlayerStats count={this.props.spins} life />
                <View style={[styles.bodyWidth, styles.whiteBackground]}>
                    <Text
                        style={{ textAlign: 'center', fontSize: 30, fontWeight: 'bold', color: 'white', padding: 20 }}>{language.spinWheel}</Text>
                    <View style={{ flex: 3 }}>
                        <Animated.Image
                            source={require('../assets/images/spinner/subjects.png')}
                            style={{
                                flex: 1,
                                height: undefined,
                                width: undefined,
                                resizeMode: 'contain',
                                margin: 26,
                                zIndex: -1,
                                position: 'absolute',
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0,
                                transform: [{
                                    rotate: spin
                                }]
                            }}
                        />

                        <Image
                            source={require('../assets/images/spinner/circumference.png')}
                            style={{
                                flex: 1,
                                height: undefined,
                                width: undefined,
                                resizeMode: 'contain',
                                margin: 15,
                                zIndex: -1,
                                position: 'absolute',
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0
                            }}
                        />


                        <TouchableOpacity
                            style={{ flex: 1, margin: 115, zIndex: 1 }} onPress={() => this._spin()}
                        >
                            <Image source={require('../assets/images/spinner/center.png')}
                                style={{ flex: 1, height: undefined, width: undefined, resizeMode: 'contain' }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1.1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{
                            flex: 1,
                            marginTop: 30,
                            marginBottom: 10
                        }}>
                            <TouchableOpacity style={{ flex: 1 }}>
                                <Image source={require('../assets/images/common/spin.png')}
                                    style={{ height: undefined, width: undefined, resizeMode: 'contain', flex: 2 }}
                                >
                                    <Text style={{
                                        marginTop: 16,
                                        color: 'white',
                                        textAlign: 'center',
                                        fontWeight: 'bold'
                                    }}>{this.props.spins}</Text>
                                </Image>
                                <Text style={{ flex: 1, color: 'white', textAlign: 'center', fontWeight: 'bold' }}>{language.spinsLeft}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            flex: 1,
                            marginTop: 30,
                            marginBottom: 10
                        }}>
                            <TouchableOpacity style={{ flex: 1 }} onPress={() => this._spin()}>
                                <Image source={require('../assets/images/spinner/spin-again.png')}
                                    style={{ height: undefined, width: undefined, resizeMode: 'contain', flex: 2 }}
                                />
                                <Text style={{ flex: 1, color: 'white', textAlign: 'center', fontWeight: 'bold' }}>{language.spinAgain}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, marginTop: 30, marginBottom: 10, marginRight: 10 }}>
                            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.doLoad('shop')}>
                                <Image source={require('../assets/images/common/coin.png')}
                                    style={{ height: undefined, width: 80, resizeMode: 'contain', flex: 2, marginLeft: 20 }}
                                />
                                <Text style={{ flex: 1, color: 'white', textAlign: 'center', fontWeight: 'bold' }}>{language.buySpin}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1
    },
    bodyWidth: {
        flex: 18
    },
    whiteBackground: {
        backgroundColor: '#2B292A'
    }
});

