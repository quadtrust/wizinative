import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
    ToastAndroid
} from 'react-native';
import Header from './Header';
import Button from 'react-native-button';
import { CheckboxField, Checkbox } from 'react-native-checkbox-field';;
import language from './Language';
import API from './API';
import Sounds from './Sounds';

export default class Setting extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fieldLabel: 'Field A',
            sound: false,
            vibration: false,
            notifications: false,
            notification_sounds: false,
            notification_vibration: false
        };

        // this.selectCheckbox = this.selectCheckbox.bind(this);
    }

    componentDidMount() {
        AsyncStorage.getItem('user').then((response) => {
            let settings = JSON.parse(response).setting;
            if (settings.sound == 1) {
                this.selectCheckbox(1);
            }
            if (settings.vibration == 1) {
                this.selectCheckbox(2);
            }
            if (settings.notifications == 1) {
                this.selectCheckbox(3);
            }
            if (settings.notification_sounds == 1) {
                this.selectCheckbox(4);
            }
            if (settings.notification_vibration == 1) {
                this.selectCheckbox(5);
            }
        });
    }

    selectCheckbox(id) {
        switch (id) {
            case 1:
                this.setState({ sound: !this.state.sound });
                break;
            case 2:
                this.setState({ vibration: !this.state.vibration });
                break;
            case 3:
                this.setState({ notifications: !this.state.notifications });
                break;
            case 4:
                this.setState({ notification_sounds: !this.state.notification_sounds });
                break;
            case 5:
                this.setState({ notification_vibration: !this.state.notification_vibration });
                break;
        }
    }

    changeSettings() {
        let setting = {
            sound: 0,
            vibration: 0,
            notifications: 0,
            notification_sounds: 0,
            notification_vibration: 0
        }
        if (this.state.sound) {
            Sounds.playBackground();
            setting.sound = 1;
        } else {
            Sounds.stopBackground();
        }
        if (this.state.vibration) {
            setting.vibration = 1;
        }
        if (this.state.notifications) {
            setting.notifications = 1;
        }
        if (this.state.notification_sounds) {
            setting.notification_sounds = 1;
        }
        if (this.notification_vibration) {
            setting.notification_vibration = 1;
        }
        API.updateSetting(setting, this.props.doResetTo, (resp) => {
            if (resp.success) {
                AsyncStorage.getItem('user').then((sUser) => {
                    let user = JSON.parse(sUser);
                    user.setting = resp.setting;
                    AsyncStorage.setItem('user', JSON.stringify(user));
                    ToastAndroid.show('Settings updated', ToastAndroid.SHORT);
                });
            }
        });
    }


    render() {
        const defaultColor = '#fff';
        return (
            <View style={styles.fullWidth}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true} />
                <View style={styles.bodyWidth}>

                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>

                        <Text style={[{ fontSize: 30, color: '#4B82C3', fontWeight: '800', marginTop: 25 }]}>
                            {language.settings}
                        </Text>
                    </View>
                    <View style={[styles.checkContainer]}>
                        <TouchableOpacity>
                            <CheckboxField
                                label={this.state.fieldLabel}
                                onSelect={() => {
                                    this.selectCheckbox(1)
                                }}
                                selected={this.state.sound}
                                defaultColor={defaultColor}
                                selectedColor="#61C6B4"
                                checkboxStyle={styles.checkboxStyle}>
                                <Text style={{ color: defaultColor }}>✓</Text>
                            </CheckboxField>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => {
                            this.selectCheckbox(1)
                        }}>
                            <Text style={[styles.checkText]}>
                                {language.sound}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.checkContainer]}>
                        <CheckboxField
                            label={this.state.fieldLabel}
                            onSelect={() => {
                                this.selectCheckbox(2)
                            }}
                            selected={this.state.vibration}
                            defaultColor={defaultColor}
                            selectedColor="#61C6B4"
                            checkboxStyle={styles.checkboxStyle}>
                            <Text style={{ color: defaultColor }}>✓</Text>
                        </CheckboxField>
                        <TouchableOpacity onPress={() => {
                            this.selectCheckbox(2)
                        }}>

                            <Text style={[styles.checkText]}>
                                {language.vibration}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.checkContainer]}>
                        <CheckboxField
                            label={this.state.fieldLabel}
                            onSelect={() => {
                                this.selectCheckbox(3)
                            }}
                            selected={this.state.notifications}
                            defaultColor={defaultColor}
                            selectedColor="#61C6B4"
                            checkboxStyle={styles.checkboxStyle}>
                            <Text style={{ color: defaultColor }}>✓</Text>
                        </CheckboxField>
                        <TouchableOpacity onPress={() => {
                            this.selectCheckbox(3)
                        }}>
                            <Text style={[styles.checkText]}>
                                {language.notifications}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.checkContainer]}>
                        <CheckboxField
                            label={this.state.fieldLabel}
                            onSelect={() => {
                                this.selectCheckbox(4)
                            }}
                            selected={this.state.notification_sounds}
                            defaultColor={defaultColor}
                            selectedColor="#61C6B4"
                            checkboxStyle={styles.checkboxStyle}>
                            <Text style={{ color: defaultColor }}>✓</Text>
                        </CheckboxField>
                        <TouchableOpacity onPress={() => {
                            this.selectCheckbox(4)
                        }}>
                            <Text style={[styles.checkText]}>
                                {language.notification_sound}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.checkContainer]}>
                        <CheckboxField
                            label={this.state.fieldLabel}
                            onSelect={() => {
                                this.selectCheckbox(5)
                            }}
                            selected={this.state.notification_vibration}
                            defaultColor={defaultColor}
                            selectedColor="#61C6B4"
                            checkboxStyle={styles.checkboxStyle}>
                            <Text style={{ color: defaultColor }}>✓</Text>
                        </CheckboxField>
                        <TouchableOpacity onPress={() => {
                            this.selectCheckbox(5)
                        }}>
                            <Text style={[styles.checkText]}>
                                {language.notification_vibration}
                            </Text>
                        </TouchableOpacity>

                    </View>
                    <View style={[styles.checkContainer]}>

                        <TouchableOpacity onPress={() => this.props.doReplace('chooseCharacter')}>
                            <Text style={[styles.checkText]}>
                                CHANGE AVATAR
                               </Text>
                        </TouchableOpacity>

                    </View>
                    <View style={[styles.checkContainer]}>

                        <TouchableOpacity onPress={() => this.props.doLoad('chooseLanguage')}>
                            <Text style={[styles.checkText]}>
                                CHANGE LANGUAGE
                            </Text>
                        </TouchableOpacity>

                    </View>
                    <View style={[styles.center, { flexDirection: 'row' }]}>
                        <Button
                            containerStyle={{
                                backgroundColor: '#F34F4B',
                                width: 130,
                                height: 30,
                                marginTop: 20,
                                borderRadius: 5
                            }}
                            onPress={() => {
                                Sounds.stopBackground();
                                ToastAndroid.show('You have successfully logged out', ToastAndroid.SHORT);
                                AsyncStorage.removeItem("user");
                                AsyncStorage.removeItem("remember");
                                this.props.doResetTo('login');
                            }}
                            style={[styles.textCenter, { color: 'white', marginTop: 2 }]}
                        >{language.log_out}</Button>
                        <Button
                            containerStyle={{
                                backgroundColor: '#00C0FF',
                                width: 130,
                                height: 30,
                                marginTop: 20,
                                borderRadius: 5,
                                marginLeft: 5
                            }}
                            onPress={() => {
                                this.changeSettings()
                            }}
                            style={[styles.textCenter, { color: 'white', marginTop: 2 }]}
                        >SAVE</Button>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1
    },
    checkContainer: {
        borderBottomWidth: 2,
        borderColor: '#D1D2D2',
        paddingBottom: 8,
        marginTop: 8,
        flexDirection: 'row',
        alignItems: 'center'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCenter: {
        textAlign: 'center'
    },
    checkText: {
        color: '#000',
        fontSize: 20,
        fontWeight: '600',
        paddingLeft: 15,
        borderLeftWidth: 1,
        borderColor: '#D1D2D2'
    },

    bodyWidth: {
        flex: 18,
        backgroundColor: '#fff'
    },
    containerStyle: {

        padding: 20,

    },
    labelStyle: {
        flex: 1
    },
    checkboxStyle: {
        width: 36,
        height: 36,
        borderWidth: 2,
        borderColor: '#61C6B4',
        borderRadius: 1
    }

});