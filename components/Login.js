import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    ToastAndroid,
    AsyncStorage,
    TextInput,
    TouchableOpacity
} from 'react-native';
import Button from 'react-native-button';
import API from './API.js';
import language from './Language';
import FBLoginView from './FBLoginView';
import { FBLogin, FBLoginManager } from 'react-native-facebook-login';
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { CheckboxField, Checkbox } from 'react-native-checkbox-field';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            remember: false
        }
    }

    componentDidMount() {
        this._setupGoogleSignin();
        AsyncStorage.getItem('remember').then((remember) => {
            if(remember === null){
                this.props.loaded();
            } else {
                AsyncStorage.getItem('user').then((sUser) => {
                    let user = JSON.parse(sUser);
                    API.setToken(user.user.token);
                    this.props.loaded();
                    this.props.doReplace('newGame');
                })
            }
        })
    }

    async _setupGoogleSignin() {
        try {
            await GoogleSignin.hasPlayServices({ autoResolve: true });
            await GoogleSignin.configure({});

            const user = await GoogleSignin.currentUserAsync();
        }
        catch (err) {
            console.log("Play services error", err.code, err.message);
        }
    }

    _google_signIn() {
        GoogleSignin.signIn()
            .then((user) => {
                AsyncStorage.setItem('userGoogle', JSON.stringify({
                    name: user.name,
                    email: user.email
                })).then(() => {
                    this.props.doLoad('register');
                })
            })
            .catch((err) => {
                console.log('WRONG SIGNIN', err);
            })
            .done();
    }

    _facebook(profile) {
        AsyncStorage.setItem('userFb', JSON.stringify(profile)).then(() => {
            this.props.doLoad('register');
        });
    }

    _login() {
        this.props.loading();
        API.login(this.state.email, this.state.password, (resp) => {
            this.props.loaded();
            if (this.state.email == '' || this.state.password == '') {
                ToastAndroid.show('Cannot leave fields empty', ToastAndroid.SHORT)
            } else if (resp.login == false) {
                ToastAndroid.show('Check your credentials', ToastAndroid.SHORT)
            } else if (resp.user) {
                if (resp.user.language == 'ARABIC') {
                    language.setLanguage('ar');
                }
                AsyncStorage.setItem('user', JSON.stringify(resp)).then(() => {
                    if(this.state.remember){
                        AsyncStorage.setItem('remember', 'true');
                    }
                    ToastAndroid.show('You have successfully logged in', ToastAndroid.SHORT);
                    this.props.doResetTo('newGame');
                });
            } else {
                ToastAndroid.show('There was some error', ToastAndroid.SHORT);
            }
        });
    }

    focusNextField = (nextField) => {
        this.refs[nextField].focus();
    };

    render() {
        return (
            <View style={styles.fullWidth}>
                <View
                    style={[styles.fullWidth]}
                >
                    <Image
                        style={[styles.fullWidth, styles.notKnown, styles.contain, { margin: 10 }]}
                        source={require('../assets/images/wizi.png')}
                    />
                    <View style={{ flex: 3, justifyContent: 'space-between' }}>
                        <View>
                            <Button
                                containerStyle={[styles.socialButton, styles.googleColor, styles.margins]}
                                style={[styles.whiteColor, styles.smallFont]}
                                onPress={() => this._google_signIn()}
                            >{language.signInGoogle}</Button>
                            <FBLogin
                                buttonView={<FBLoginView />}
                                ref={(fbLogin) => { this.fbLogin = fbLogin }}
                                loginBehavior={FBLoginManager.LoginBehaviors.Native}
                                permissions={["email", "user_friends"]}
                                onLogin={(e) => this._facebook(e.profile)}

                            />

                        </View>
                        <View style={{
                            left: 65,
                            marginTop: 7,
                            height: 1,
                            width: 100,
                            backgroundColor: 'white',
                            position: 'absolute'
                        }} />
                        <View style={{
                            marginTop: 7,
                            height: 1,
                            width: 100,
                            backgroundColor: 'white',
                            position: 'absolute',
                            right: 65
                        }} />

                        <View >

                            <Text
                                style={[styles.blackColor, styles.center]}
                            >{language.or}</Text>
                            <Text
                                style={[styles.blackColor, styles.center, styles.someMarginTop]}
                            >{language.signInEmail}</Text>
                        </View>
                        <View>
                            <TextInput
                                style={[styles.field, styles.whiteColor, styles.margins]}
                                placeholder={language.email}
                                placeholderTextColor="white"
                                underlineColorAndroid="transparent"
                                keyboardType="email-address"
                                onChangeText={email => this.setState({ email })}
                                value={this.state.email}
                                returnKeyType="next"
                                onSubmitEditing={() => this.focusNextField('password')}

                            />
                            <TextInput
                                ref="password"
                                style={[styles.field, styles.whiteColor, styles.margins, styles.someMarginTop]}
                                placeholder={language.password}
                                secureTextEntry={true}
                                placeholderTextColor="white"
                                underlineColorAndroid="transparent"
                                value={this.state.password}
                                onChangeText={password => this.setState({ password })}
                                returnKeyType="go"
                            />

                            <View style={{flexDirection: 'row', marginHorizontal: 50}}>
                                <CheckboxField
                                    onSelect={() => {
                                        this.setState({
                                            remember: !this.state.remember
                                        });
                                    }}
                                    selected={this.state.remember}
                                    defaultColor='#fff'
                                    selectedColor="#000"
                                    containerStyle={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        paddingVertical: 20
                                    }}
                                    checkboxStyle={{width: 20,
                                        height: 20,
                                        borderWidth: 2,
                                        borderColor: '#fff',
                                        borderRadius: 5}}
                                    labelSide="right">
                                    <Text style={{ color: '#fff' }}>✓</Text>
                                </CheckboxField>
                                <View style={{justifyContent: 'center', marginLeft: 5}}>
                                    <Text style={[styles.blackColor]}>REMEMBER ME?</Text>
                                </View>
                                <TouchableOpacity onPress={() => this.props.doLoad('forgotPassword')} style={{justifyContent: 'center', alignItems: 'flex-end', flex: 1}}>
                                    <Text
                                        style={[styles.blackColor]}>{language.forgotPassword}</Text>
                                </TouchableOpacity>
                            </View>

                            <Button
                                containerStyle={[styles.socialButton, styles.margins, styles.createAccountColor, styles.someMarginTop]}
                                style={[styles.whiteColor, styles.smallFont]}
                                onPress={() => this._login()}
                            >{language.sign}</Button>
                        </View>
                        <View>
                            <Button
                                containerStyle={[styles.socialButton, styles.margins, styles.createAccountColor]}
                                style={[styles.whiteColor, styles.smallFont]}
                                onPress={() => this.props.doLoad('register')}
                            >{language.createAccount}</Button>
                            <Text
                                style={[styles.blackColor, styles.center, styles.botMar]}
                            >{language.terms}</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#2B292A'
    },
    zoomed: {
        resizeMode: 'cover'
    },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
    socialButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 20
    },
    margins: {
        marginLeft: 50,
        marginRight: 50
    },
    googleColor: {
        backgroundColor: '#d54a33'
    },
    facebookColor: {
        backgroundColor: '#3160e4'
    },
    whiteColor: {
        color: 'white'
    },
    smallFont: {
        fontSize: 12
    },
    blackColor: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 12
    },
    center: {
        textAlign: 'center'
    },
    field: {
        height: 35,
        backgroundColor: '#f44f4b',
        borderColor: 'white',
        borderWidth: 1,
        padding: 5
    },
    createAccountColor: {
        backgroundColor: '#1fd2cc'
    },
    someMarginTop: {
        marginTop: 5
    },
    botMar: {
        marginBottom: 10
    }
});
