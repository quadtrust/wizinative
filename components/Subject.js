import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    AsyncStorage,
    Dimensions,
    ToastAndroid,
    TouchableOpacity,
    ProgressBarAndroid
} from 'react-native';
import Header from './Header';
import PlayerStats from './PlayerStats';
import API from './API';
import language from './Language';
import Sounds from './Sounds';
import DialogBox from 'react-native-dialogbox';
let { height, width } = Dimensions.get('screen');

let intervalHandler;

export default class Subject extends Component {
    constructor(props) {
        super(props);

        this.state = {
            progress: 0,
            questions: false,
            i: 0,
            answered: false,
            subject: language.english,
            remark1: '',
            remark2: '',
            teacher: require('../assets/images/common/monster.png'),
            background: require('../assets/images/islam/islam.png'),
            button1: require('../assets/images/common/1.png'),
            button2: require('../assets/images/common/1.png'),
            button3: require('../assets/images/common/1.png'),
            button4: require('../assets/images/common/1.png'),
            unlockCharacter: false
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('question').then((resp) => {
            this.setState({ questions: JSON.parse(resp) });
        });
        this.runTimer();
        // this._timer();

        AsyncStorage.getItem('theme').then((resp) => {
            switch (resp) {
                case 'english':
                    this.setState({
                        background: require('../assets/images/english/english.png'),
                        subject: language.english,
                        teacher: require('../assets/images/common/english-character.png')
                    });
                    break;

                case 'maths':
                    this.setState({
                        background: require('../assets/images/maths/maths.png'),
                        subject: language.maths,
                        teacher: require('../assets/images/common/maths-character.png')
                    });
                    break;

                case 'islam':
                    this.setState({
                        background: require('../assets/images/islam/islam.png'),
                        subject: language.islamic,
                        teacher: require('../assets/images/common/islamic-character.png')
                    });
                    break;

                case 'social':
                    this.setState({
                        background: require('../assets/images/sst/sst.png'),
                        teacher: require('../assets/images/common/social-character.png'),
                        subject: language.social
                    });
                    break;

                case 'science':
                    this.setState({
                        background: require('../assets/images/science/science.png'),
                        teacher: require('../assets/images/common/science-character.png'),
                        subject: language.science
                    });
                    break;

                default:
                    this.setState({
                        background: require('../assets/images/arabic/arabic.png'),
                        teacher: require('../assets/images/common/arabic-character.png'),
                        subject: language.arabic
                    });
            }
        });
    }

    componentWillUnmount() {
        clearInterval(intervalHandler);
    }

    runTimer() {
        clearInterval(intervalHandler);
        intervalHandler = setInterval(() => {
            if (this.state.progress < 1) {
                this.setState({
                    progress: this.state.progress + 0.01
                });
            } else {
                clearInterval(intervalHandler);
            }
        }, 100);
    }

    stopTimer() {
        clearInterval(intervalHandler);
    }

    endGame() {

    }

    nextQuestion() {
        let newCounter = this.state.i + 1;
        if (newCounter > 9) {
            if(this.state.unlockCharacter){
                this.props.doReplace('unlockCharacter');
            } else {
                this.props.doReplace('result');
            }
        } else {
            this.setState({
                i: newCounter,
                progress: 0,
                answered: false
            });
            this.runTimer();
            this.resetChoices();
        }
    }

    resetChoices() {
        this.setState({
            button1: require('../assets/images/common/1.png'),
            button2: require('../assets/images/common/1.png'),
            button3: require('../assets/images/common/1.png'),
            button4: require('../assets/images/common/1.png')
        });
    }

    wrongChoice(choice) {
        switch (choice) {
            case 1:
                this.setState({
                    button1: require('../assets/images/common/3.png')
                });
                break;

            case 2:
                this.setState({
                    button2: require('../assets/images/common/3.png')
                });
                break;

            case 3:
                this.setState({
                    button3: require('../assets/images/common/3.png')
                });
                break;

            case 4:
                this.setState({
                    button4: require('../assets/images/common/3.png')
                });
                break
        }
    }

    correctChoice(choice) {
        switch (choice) {
            case 1:
                this.setState({
                    button1: require('../assets/images/common/2.png')
                });
                break;

            case 2:
                this.setState({
                    button2: require('../assets/images/common/2.png')
                });
                break;

            case 3:
                this.setState({
                    button3: require('../assets/images/common/2.png')
                });
                break;

            case 4:
                this.setState({
                    button4: require('../assets/images/common/2.png')
                });
                break
        }
    }

    checkAnswer(choice) {
        this.stopTimer();
        let flag = false;
        if (this.state.questions.questions[this.state.i].correct_choice == choice) {
            flag = true;
        }

        if (flag) {
            this.correctChoice(choice);
            Sounds.playRightAnswer();
            if(this.state.i === 0) {
                this.setState({
                    unlockCharacter: true
                });
            }
        } else {
            this.wrongChoice(choice);
            this.correctChoice(parseInt(this.state.questions.questions[this.state.i].correct_choice));
            Sounds.playWrongAnswer();
            this.setState({
                unlockCharacter: false
            });
        }

        API.nextQuestion(this.state.questions.questions[this.state.i].GameQuestionId, { choice: choice }, this.props.doResetTo, (data) => { });

        this.setState({
            answered: true,
            remark1: flag ? language.excellent : language.too_bad,
            remark2: flag ? language.correct_answers : language.wrong_answer
        });
    }

    end_game() {
        let question_id = this.state.questions ? this.state.questions.questions[this.state.i].GameQuestionId : 0;
        this.dialogbox.confirm({
            title: 'End Game',
            content: 'Do you really want to end game?',
            ok: {
                text: 'Yes',
                callback: () => {
                    API.end_game(() => {
                    }, question_id, (resp) => {
                        if (resp.status) {
                            this.props.doResetTo('result');
                        } else {
                            ToastAndroid.show('Some error occurred', ToastAndroid.SHORT);
                        }
                    });
                }
            }
        });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} doResetTo={this.props.doResetTo}
                    endGame={true} end_game={() => this.end_game()} />
                <PlayerStats life={this.props.lives} count="1" />
                <View style={[styles.bodyWidth]}>
                    <View style={[{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        zIndex: this.state.progress >= 1 ? 5 : -2,
                        backgroundColor: 'rgba(0,0,0,0.65)',
                        height: height,
                        width: width
                    }]}>
                        <TouchableOpacity style={{ flex: 2 }} onPress={() => this.nextQuestion()}>
                            <Image
                                style={{ flex: 1, height: undefined, width: undefined, resizeMode: 'contain' }}
                                source={require('../assets/images/timer.png')}
                            />
                        </TouchableOpacity>
                        <View style={{ flex: 1 }}></View>
                    </View>

                    <ProgressBarAndroid color="red" progress={this.state.progress} style={{ height: 3 }} styleAttr="Horizontal" indeterminate={false} />
                    <Image style={[styles.fullWidth, styles.zoomed, styles.notKnown]} source={this.state.background} >
                        <Image style={styles.blackBoard} source={require('../assets/images/common/board.png')} >
                            <Text style={styles.headerText}> {this.state.subject} </Text>
                            <Text style={styles.subHeading}> {this.state.questions ? this.state.questions.questions[this.state.i].question : ''}</Text>
                            <View style={{ flex: 1, marginBottom: 20 }}>
                                <TouchableOpacity disabled={this.state.answered} onPress={() => this.checkAnswer(1)} style={styles.flexContainer}>
                                    <Image style={styles.choiceBackground} source={this.state.button1} >
                                        <Text style={styles.choice}> {this.state.questions ? this.state.questions.questions[this.state.i].choice1 : ''} </Text>
                                    </Image>
                                </TouchableOpacity>
                                <TouchableOpacity disabled={this.state.answered} onPress={() => this.checkAnswer(2)} style={styles.flexContainer}>
                                    <Image style={styles.choiceBackground} source={this.state.button2} >
                                        <Text style={styles.choice}> {this.state.questions ? this.state.questions.questions[this.state.i].choice2 : ''} </Text>
                                    </Image>
                                </TouchableOpacity>
                                <TouchableOpacity disabled={this.state.answered} onPress={() => this.checkAnswer(3)} style={styles.flexContainer}>
                                    <Image style={styles.choiceBackground} source={this.state.button3} >
                                        <Text style={styles.choice}> {this.state.questions ? this.state.questions.questions[this.state.i].choice3 : ''} </Text>
                                    </Image>
                                </TouchableOpacity>
                                <TouchableOpacity disabled={this.state.answered} onPress={() => this.checkAnswer(4)} style={styles.flexContainer}>
                                    <Image style={styles.choiceBackground} source={this.state.button4} >
                                        <Text style={styles.choice}> {this.state.questions ? this.state.questions.questions[this.state.i].choice4 : ''} </Text>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </Image>
                        <Image source={this.state.teacher}
                            style={{ height: 100, width: 100, left: 10, position: 'absolute', resizeMode: 'contain', zIndex: 2, opacity: this.state.answered ? 1 : 0 }}
                        />
                        <View style={{ flex: 2, marginTop: 10, opacity: this.state.answered ? 1 : 0 }}>
                            <TouchableOpacity onPress={() => this.nextQuestion()} style={{flex: 1}}>
                                <View style={{ flex: 3, backgroundColor: 'rgba(0,0,0,0.65)', justifyContent: 'space-between' }}>
                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                        <View style={{ flex: 2 }}><Text /></View>
                                        <View style={{ flex: 5 }}><Text
                                            style={{
                                                fontSize: 24,
                                                color: 'orange',
                                                fontWeight: '800',
                                                textAlign: 'center'
                                            }}>{this.state.remark1}</Text></View>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                        <View style={{ flex: 2 }}><Text /></View>
                                        <View style={{ flex: 5 }}><Text style={{
                                            fontSize: 24,
                                            color: '#fff',
                                            fontWeight: '800',
                                            textAlign: 'center',
                                            borderColor: 'white',
                                            lineHeight: 21
                                        }}>{this.state.remark2}</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ flex: 1, flexDirection: 'row-reverse', opacity: this.state.answered ? 1 : 0, paddingBottom: 5 }}>
                                    <View style={{ flex: 1, marginTop: 5 }}>
                                        <Image style={{ height: undefined, width: undefined, flex: 1, resizeMode: 'contain' }}
                                               source={require('../assets/images/common/next.png')} />
                                    </View>
                                    <View>
                                        <Text style={[styles.nextText]}>{language.next_question}</Text>
                                    </View>
                                    <View style={{ flex: 2 }}></View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </Image>
                </View>
                <DialogBox ref={(dialogbox) => { this.dialogbox = dialogbox }} />
            </View>


        );
    }
}
const styles = StyleSheet.create({
    subjectContainer: {
        flex: 1,
    },
    questionsAsked: {
        flex: 1,
        alignItems: 'center',
    },
    fullWidth: {
        flex: 1,
        backgroundColor: 'white'
    },
    nextText: {
        fontWeight: '800',
        fontSize: 16,
        color: '#fff'
    },
    teacherLogo: {
        resizeMode: 'contain',
        height: 120,
        width: 130,
        zIndex: 1000
    },
    choice: {
        textAlign: 'center',
        color: '#000',
        fontWeight: 'bold',
        fontSize: 16
    },
    subHeading: {
        fontSize: 16,
        marginLeft: 40,
        marginRight: 40,
        color: "#fff",
        fontWeight: 'bold',
        textAlign: "center",
    },
    choiceBackground: {
        height: undefined,
        width: undefined,
        flex: 1,
        resizeMode: 'contain',
        alignItems: 'center',
        justifyContent: 'center'
    },
    flexContainer: {
        flex: 1,
        marginLeft: 50,
        marginRight: 50
    },
    bodyWidth: {
        flex: 18
    },
    whiteBackground: {
        backgroundColor: "white"
    },
    blackBoard: {
        height: null,
        width: null,
        flex: 8,
        resizeMode: 'contain',
        margin: 20
    },
    boardContainer: {
        flex: 12,

    },
    headerText: {
        fontSize: 25,
        color: '#fff',
        fontWeight: "700",
        textAlign: 'center',
        marginTop: 20,
        textDecorationLine: "underline",
    },
    choiceBoard: {
        flex: 4,
    },
    zoomed: {
        resizeMode: 'cover'
    },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    }
});