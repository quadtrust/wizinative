import React, {Component} from 'react'
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image
} from 'react-native'
import API from './API'
import Header from './Header'
import language from './Language'
export default class Statistics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tests_attempted: '',
            correct_answers: '',
            wizi_coins: 0,
            invites: ''

        }
    }

    componentDidMount() {

        API.get_statistics(this.props.doResetTo, (resp) => {
            this.setState({tests_attempted: resp.statistics.games_played});
            this.setState({correct_answers: resp.statistics.correct_answers});
            this.setState({invites: resp.statistics.invites})
        })
    }

    render() {
        return (
            <View style={styles.fullWidth}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true}/>
                <View style={styles.bodyWidth}>
                    <Text style={[styles.heading]}>{language.statistic}</Text>
                    <ScrollView>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/hat.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.testsAttempted}</Text>
                            </View>
                            <View style={[styles.scoreView]}>
                                <Text style={[styles.score]}>{this.state.tests_attempted}</Text>
                            </View>

                        </View>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/trophy.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.correctAnswers}</Text>
                            </View>
                            <View style={[styles.scoreView]}>
                                <Text style={[styles.score]}>{this.state.correct_answers}</Text>
                            </View>
                        </View>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/heart.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.livesLeft}</Text>
                            </View>
                            <View style={[styles.scoreView]}>
                                <Text style={[styles.score]}>{this.props.lives}</Text>
                            </View>
                        </View>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/spin.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.spinsLeft}</Text>
                            </View>
                            <View style={[styles.scoreView]}>
                                <Text style={[styles.score]}>{this.props.spins}</Text>
                            </View>
                        </View>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/coin.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.wiziCoins}</Text>
                            </View>
                            <View style={[styles.scoreView]}>
                                <Text style={[styles.score]}>{this.state.wizi_coins}</Text>
                            </View>
                        </View>

                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/invite.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.numberInvites}</Text>
                            </View>
                            <View style={[styles.scoreView]}>
                                <Text style={[styles.score]}>{this.state.invites}</Text>
                            </View>
                        </View>
                    </ScrollView>
                </View>


            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#fff'
    },
    statsContainer: {
        height: 70,
        marginTop: 15,
        flex: 1,
        flexDirection: 'row',
        paddingBottom: 8,
        borderBottomWidth: 2,
        borderColor: '#7A7B7C'
    },
    scoreView: {flex: 2, justifyContent: 'center'},
    attempView: {flex: 4, justifyContent: 'center', borderLeftWidth: 1, borderColor: '#7D7E7F'},
    iconView: {flex: 2, justifyContent: 'center', alignItems: 'center'},
    iconSize: {height: 60, width: 70, resizeMode: 'contain'},
    attemp: {textAlign: 'left', marginLeft: 12, color: '#000102', fontSize: 18, fontWeight: 'bold'},
    score: {textAlign: 'center', fontSize: 25, fontWeight: '600', color: '#6ADC97'},
    heading: {
        fontSize: 28,
        fontWeight: '700',
        color: '#2895D0',
        textAlign: 'center',
        marginTop: 10

    },

    bodyWidth: {
        flex: 18
    }
});