import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    AsyncStorage
} from 'react-native'
import styles from './Style'
import Header from  './Header'

export default class SuggestQuestion extends Component {
    _loadQuestion(subject) {
        switch (subject) {
            case 'english':
                AsyncStorage.setItem('theme', 'english');
                break;
            case 'maths':
                AsyncStorage.setItem('theme', 'maths');
                break;
            case 'islamic':
                AsyncStorage.setItem('theme', 'islam');
                break;
            case 'arabic':
                AsyncStorage.setItem('theme', 'arabic');
                break;
            case 'science':
                AsyncStorage.setItem('theme', 'science');
                break;
            case 'social':
                AsyncStorage.setItem('theme', 'social');
                break;
        }
        this.props.doReplace('askQuestion');
    }

    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} BACK={true}/>
                <View style={[styles.bodyWidth, styles.whiteBack]}>
                    <View style={[, {backgroundColor: '#21AF96'}]}>
                        <Text style={[styles.center, {color: 'white', fontSize: 23, fontWeight: '700'}]}>
                            SUGGEST QUESTION
                        </Text>
                    </View>
                    <View>
                        <Text style={[styles.center, {color: '#1CD9FF', fontWeight: '700', fontSize: 23}]}>
                            SELECT YOUR CATEGORY
                        </Text>
                    </View>
                    <View style={[, {backgroundColor: '#3C434C'}]}>
                        <Text style={[styles.center, {color: 'white', fontSize: 18, fontWeight: '600'}]}>CATEGORY WITH
                            FEWER QUESTIONS</Text>
                    </View>
                    <View
                        style={[styles.row, {justifyContent: 'center', borderBottomWidth: 1, borderColor: '#ABADAD', flex: 1}]}>
                        <View style={{
                            borderRightWidth: 1,
                            borderColor: '#ABADAD',
                            flex: 1,
                            padding: 15
                        }}>
                            <TouchableOpacity onPress={() => this._loadQuestion('maths')} style={{flex: 1}}>
                                <Image style={{resizeMode: 'contain', flex: 1, height: undefined, width: undefined}}
                                       source={require('../assets/images/common/maths-character.png')}/>
                                <Text style={[styles.center, {
                                    fontSize: 16,
                                    color: '#FF920D',
                                    fontWeight: '700'
                                }]}>MATHS</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            borderRightWidth: 1,
                            borderColor: '#ABADAD',
                            flex: 1,
                            padding: 15
                        }}>
                            <TouchableOpacity onPress={() => this._loadQuestion('islamic')} style={{flex: 1}}>
                                <Image style={{resizeMode: 'contain', flex: 1, height: undefined, width: undefined}}
                                       source={require('../assets/images/common/islamic-character.png')}/>
                                <Text style={[styles.center, {fontSize: 16, color: '#FF920D', fontWeight: '700'}]}>ISLAMIC
                                    STUDIES</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 1, padding: 15}}>
                            <TouchableOpacity onPress={() => this._loadQuestion('arabic')} style={{flex: 1}}>
                                <Image style={{
                                    resizeMode: 'contain',
                                    flex: 1,
                                    height: undefined,
                                    width: undefined
                                }} source={require('../assets/images/common/arabic-character.png')}/>
                                <Text style={[styles.center, {fontSize: 16, color: '#FF920D', fontWeight: '700'}]}>ARABIC</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[{backgroundColor: '#3C434C', marginTop: 1, marginBottom: 1}]}>
                        <Text style={[styles.center, {color: 'white', fontSize: 18, fontWeight: '600'}]}>OTHER
                            CATEGORY</Text>
                    </View>
                    <View
                        style={[styles.row, {justifyContent: 'center', borderBottomWidth: 1, borderColor: '#ABADAD', flex: 1}]}>
                        <View style={{
                            borderRightWidth: 1,
                            borderColor: '#ABADAD',
                            flex: 1,
                            padding: 15
                        }}>
                            <TouchableOpacity onPress={() => this._loadQuestion('science')} style={{flex: 1}}>
                                <Image style={{resizeMode: 'contain', flex: 1, height: undefined, width: undefined}}
                                       source={require('../assets/images/common/science-character.png')}/>
                                <Text style={[styles.center, {fontSize: 16, color: '#FF920D', fontWeight: '700'}]}>SCIENCE</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            borderRightWidth: 1,
                            borderColor: '#ABADAD',
                            flex: 1,
                            padding: 15
                        }}>
                            <TouchableOpacity onPress={() => this._loadQuestion('social')} style={{flex: 1}}>
                                <Image style={{resizeMode: 'contain', flex: 1, height: undefined, width: undefined}}
                                       source={require('../assets/images/common/social-character.png')}/>
                                <Text style={[styles.center, {fontSize: 16, color: '#FF920D', fontWeight: '700'}]}>SOCIAL
                                    STUDIES</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 1, padding: 15}}>
                            <TouchableOpacity onPress={() => this._loadQuestion('english')} style={{flex: 1}}>
                                <Image style={{resizeMode: 'contain', flex: 1, height: undefined, width: undefined}}
                                       source={require('../assets/images/common/english-character.png')}/>
                                <Text style={[styles.center, {fontSize: 16, color: '#FF920D', fontWeight: '700'}]}>ENGLISH</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}