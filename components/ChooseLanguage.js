import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';
import styles from './Style';
import Header from './Header';
import Button from 'react-native-button';
import API from './API';
import Language from './Language';

export default class ChooseLanguage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            english: '',
            arabic: '',
            englishStyle: {},
            arabicStyle: {}
        };
    }

    selectEnglish() {
        this.setState({
            englishStyle: { backgroundColor: 'rgba(0, 0, 0, 0.6)' },
            arabicStyle: {},
            english: true,
            arabic: false
        });
    }

    selectArabic() {
        this.setState({
            arabicStyle: { backgroundColor: 'rgba(0, 0, 0, 0.6)' },
            englishStyle: {},
            arabic: true,
            english: false
        });
    }

    changeLanguage() {
        let language = null;
        if (this.state.english) {
            language = 'ENGLISH';
        } else if (this.state.arabic) {
            language = 'ARABIC';
        }
        API.changeLanguage(language, this.props.doResetTo, (resp) => {
            if (resp.success) {
                AsyncStorage.getItem('user').then((sUser) => {
                    let user = JSON.parse(sUser);
                    user.user.language = language;
                    AsyncStorage.setItem('user', JSON.stringify(user));
                    if (language == 'ENGLISH') {
                        Language.setLanguage('en');
                    } else if (language == 'ARABIC') {
                        Language.setLanguage('ar');
                    }
                    this.props.doPop();
                });
            }
        });
    }

    componentDidMount() {
        AsyncStorage.getItem('user').then((resp) => {
            let user = JSON.parse(resp);
            if (user.user.language == 'ENGLISH') {
                this.setState({
                    englishStyle: { backgroundColor: 'rgba(0, 0, 0, 0.6)' },
                    english: true
                });
            } else {
                this.setState({
                    arabicStyle: { backgroundColor: 'rgba(0, 0, 0, 0.6)' },
                    arabic: true
                });
            }
        });
    }

    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} player={true} />
                <View style={[styles.bodyWidth, styles.whiteBack]}>
                    <Image
                        style={[styles.fullWidth, styles.zoomed, styles.notKnown]}
                        source={require('../assets/images/language/back.png')}
                    >
                        <View style={[styles.bodyWidth, { justifyContent: 'center' }]} >
                            <View>
                                <Text style={[styles.languageHeader]}>
                                    CHOOSE YOUR LANGUAGE
                                </Text>
                            </View>
                            <View style={[styles.row, { justifyContent: 'space-around', marginTop: 30, marginBottom: 20 }]}>
                                <View style={[{ padding: 5 }, this.state.englishStyle]}>
                                    <TouchableOpacity onPress={() => this.selectEnglish()}>
                                        <Image style={{ resizeMode: 'contain', height: 80, width: 80 }} source={require('../assets/images/language/english.png')} />
                                        <Text style={{ fontWeight: '700', color: 'white', fontSize: 20 }}>ENGLISH</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={[{ padding: 5 }, this.state.arabicStyle]}>
                                    <TouchableOpacity onPress={() => this.selectArabic()}>
                                        <Image style={{ resizeMode: 'contain', height: 80, width: 80 }} source={require('../assets/images/language/arabic.png')} />
                                        <Text style={{ fontWeight: '700', color: 'white', fontSize: 20 }}>ARABIC</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <Button
                                    containerStyle={[{
                                        marginTop: 20,
                                        padding: 5,
                                        width: 100,
                                        borderRadius: 5,
                                        backgroundColor: '#EC0005',
                                    }]}
                                    style={[{ fontSize: 18, color: 'white', fontWeight: '900' }]}
                                    onPress={() => this.changeLanguage()}
                                >CHANGE</Button>
                            </View>
                        </View>
                    </Image>
                </View>
            </View>
        );
    }
}