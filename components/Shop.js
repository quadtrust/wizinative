import React, {Component} from 'react'
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image
} from 'react-native'
import Header from './Header'
import Button from 'react-native-button'
import language from './Language'
export default class Shop extends Component {

    render() {
        return (
            <View style={styles.fullWidth}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true} />
                <View style={styles.bodyWidth}>
                    <Text style={[styles.heading, styles.backColor]}>{language.shop}</Text>
                    <ScrollView>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/heart.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.lives}</Text>
                            </View>
                            <Button
                                containerStyle={[styles.buttonBody]}
                                style={[styles.textCenter,styles.buttonText]}
                            >{language.kwd1}
                            </Button>
                            {/*<View style={[styles.scoreView]}>
                                <Text style={[styles.score]}>210</Text>
                            </View>*/}

                        </View>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/spin.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.spins}</Text>
                            </View>
                            <Button
                                containerStyle={[styles.buttonBody]}
                                style={[styles.textCenter, styles.buttonText]}
                            >{language.kwd2}
                            </Button>
                        </View>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/coin.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.coinWizi}</Text>
                            </View>
                            <Button
                                containerStyle={[styles.buttonBody]}
                                style={[styles.textCenter, styles.buttonText]}
                            >{language.kwd2}
                            </Button>
                        </View>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/premium.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.premiumVersion}</Text>
                            </View>
                            <Button
                                containerStyle={[styles.buttonBody]}
                                style={[styles.textCenter, styles.buttonText]}
                            >{language.kwd3}
                            </Button>
                        </View>
                        <View style={[styles.statsContainer]}>
                            <View style={[styles.iconView]}>
                                <Image
                                    style={[styles.iconSize]}
                                    source={require('../assets/images/common/time.png')}
                                >
                                </Image>
                            </View>
                            <View
                                style={[styles.attempView]}>
                                <Text style={[styles.attemp]}>{language.extraTime}</Text>
                            </View>
                            <Button
                                containerStyle={[styles.buttonBody]}
                                style={[styles.textCenter, styles.buttonText]}
                            >{language.kwd}
                            </Button>
                        </View>


                    </ScrollView>
                </View>


            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor:'#fff'
    },
    backColor:{
      backgroundColor:'#1EBBA7'
    },
    statsContainer:{
        height: 70,
        marginTop:15,
        flex: 1,
        flexDirection: 'row',
        paddingBottom: 8,
        borderBottomWidth: 2,
        borderColor: '#7A7B7C'
    },
    scoreView:{flex: 2, justifyContent: 'center'},
    attempView:{flex: 4, justifyContent: 'center', borderLeftWidth: 1, borderColor: '#7D7E7F'},
    iconView:{flex: 2, justifyContent: 'center', alignItems: 'center'},
    iconSize: {height: 60, width: 70, resizeMode:'contain'},
    attemp: {textAlign: 'left',marginLeft:12, color: '#000102', fontSize: 18, fontWeight: 'bold'},
    score: {textAlign: 'center', fontSize: 25, fontWeight: '600', color: '#6ADC97'},
    heading: {
        fontSize: 30,
        fontWeight: '700',
        color: '#fff',
        textAlign: 'center'

    },
    buttonBody: {backgroundColor:'#1EBBA7', width:80, height:30,marginTop:20,borderRadius:5, marginRight:10},
    buttonText: {color:'white',marginTop:5, fontSize:13, fontWeight:'bold'},
    bodyWidth: {
        flex: 18
    }
});