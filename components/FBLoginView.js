import React, { Component } from 'react';
import { StyleSheet,Text,View } from 'react-native';
import language from './Language';
import Button from 'react-native-button'


export default class FBLoginView extends Component {
    static contextTypes = {
        isLoggedIn: React.PropTypes.bool,
        login: React.PropTypes.func,
        logout: React.PropTypes.func,
        props: React.PropTypes.object
    };

    constructor(props) {
        super(props);
    }

    render(){
        return (
            <View style={[]}>
                <Button onPress={() => {
                if(!this.context.isLoggedIn){
                  this.context.login()
                }else{
                  this.context.logout()
                }

              }} containerStyle={[styles.socialButton, styles.facebookColor, styles.margins]}
                        style={[styles.whiteColor, styles.smallFont]}
                >{language.signInFacebook}</Button>

            </View>
        )
    }
}

const styles = StyleSheet.create({

    socialButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 20
    },
    margins: {
        marginLeft: 50,
        marginRight: 50
    },

    facebookColor: {
        backgroundColor: '#3160e4'
    },
    whiteColor: {
        color: 'white'
    },
    smallFont: {
        fontSize: 12
    }

});

