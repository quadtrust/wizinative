import React, { Component, PropTypes } from 'react';
import {
    Text
} from 'react-native';
import Login from './Login';
import Spinner from './Spinner';
import Subject from './Subject';
import Result from './Result';
import Statistics from './Statistics';
import Help from './Help';
import Menu from './Menu';
import Header from './Header';
import Social from './Social';
import Setting from './Setting';
import Shop from './Shop';
import Question from './Question';
import Invite from './Invite';
import SubjectSelection from './SubjectSelection';
import Profile from './Profile';
import Selected from './Selected';
import Register from './Register';
import ChooseCharacter from './ChooseCharacter';
import CharacterSelected from './CharacterSelected';
import Challenge from './Challenge';
import NewGame from './NewGame';
import InviteFriend from './InviteFriend';
import Compeitition from './Competition';
import Lives from './Lives';
import ChooseLanguage from './ChooseLanguage';
import SuggestQuestion from './SuggestQuestion';
import AskQuestion from './AskQuestion';
import ForgotPassword from './ForgotPassword';
import RandomSpinner from './RandomSpinner';
import SubjectVersus from './SubjectVersus';
import Directorates from './Directorates';
import TopDirectoratePlayers from './TopDirectoratePlayers';
import UnlockCharacter from './UnlockCharacter';
import CharacterUnlocked from './CharacterUnlocked';
import CheckStatus from './CheckStatus';

export default class Scene extends Component {
    static propTypes = {
        name: PropTypes.string.isRequired,
        doLoad: PropTypes.func.isRequired,
        doReplace: PropTypes.func.isRequired,
        doResetTo: PropTypes.func.isRequired,
        doPop: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.setState({});
    }

    render() {
        let pages = {
            login: <Login {...this.props} />,
            spinner: <Spinner {...this.props} />,
            subject: <Subject {...this.props} />,
            statistics: <Statistics {...this.props} />,
            result: <Result {...this.props} />,
            header: <Header {...this.props} />,
            help: <Help {...this.props} />,
            menu: <Menu {...this.props} />,
            social: <Social {...this.props} />,
            setting: <Setting {...this.props} />,
            shop: <Shop {...this.props} />,
            subjectSelection: <SubjectSelection {...this.props} />,
            question: <Question {...this.props} />,
            invite: <Invite {...this.props} />,
            profile: <Profile {...this.props} />,
            selected: <Selected {...this.props} />,
            register: <Register {...this.props} />,
            chooseCharacter: <ChooseCharacter {...this.props} />,
            characterSelected: <CharacterSelected {...this.props} />,
            challenge: <Challenge {...this.props} />,
            newGame: <NewGame {...this.props} />,
            inviteFriend: <InviteFriend {...this.props} />,
            competition: <Compeitition {...this.props} />,
            lives: <Lives {...this.props} />,
            chooseLanguage: <ChooseLanguage {...this.props} />,
            suggestQuestion: <SuggestQuestion {...this.props} />,
            askQuestion: <AskQuestion {...this.props} />,
            forgotPassword: <ForgotPassword {...this.props} />,
            randomSpinner: <RandomSpinner {...this.props} />,
            subjectVersus: <SubjectVersus {...this.props} />,
            directorates: <Directorates {...this.props} />,
            topDirectoratePlayers: <TopDirectoratePlayers {...this.props} />,
            unlockCharacter: <UnlockCharacter {...this.props} />,
            characterUnlocked: <CharacterUnlocked {...this.props} />,
            checkStatus: <CheckStatus {...this.props} />
        };
        return (pages[this.props.name] ? pages[this.props.name] : <Text>View not found - {this.props.name}</Text>);
    }
}