import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native'
import Header from './Header'
import language from './Language'
export default class Question extends Component {

    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true}/>
                <View style={[styles.bodyWidth]}>
                    <View style={[styles.questionContainer,{marginTop:70}]}>
                        <Text style={[styles.headline]}>
                            {language.addQuestion}
                        </Text>
                        <TouchableOpacity onPress={() => this.props.doLoad('suggestQuestion')}>
                            <Text style={[styles.subHeader]}>{language.haveQuestion}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.questionContainer]}>
                        <Text style={[styles.headline]}>
                            {language.checkStatus}
                        </Text>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.loading();
                                this.props.doLoad('checkStatus');
                            }}
                        >
                            <Text style={[styles.subHeader]}>{language.checkMessagestatus}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.questionContainer]}>
                        <Text style={[styles.headline]}>
                            {language.rateQuestion}
                        </Text>
                        <TouchableOpacity>
                            <Text style={[styles.subHeader]}>
                                {language.rateQuestionDatabase}
                            </Text>
                        </TouchableOpacity>
                    </View>


                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1
    },
    questionContainer: {

        borderBottomWidth:2,
        borderColor:'white',
        paddingBottom:30,
        marginTop:20
    },
    contain: {
        resizeMode: 'contain'
    },
    headline: {
        fontSize: 25,
        marginLeft: 20,
        marginRight: 20,
        color: 'orange',
        fontWeight: '800'
    },
    subHeader: {
        fontSize: 23,
        marginLeft: 20,
        marginRight: 20,
        fontWeight: '800',
        color: 'white'
    },
    socialButtonSize: {
        width: 180, height: 50, marginTop: 15
    },
    bodyWidth: {
        flex: 18,
        backgroundColor: '#2B292A'
    },
    row: {
        flexDirection: 'row'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCenter: {
        textAlign: 'center'
    }
});