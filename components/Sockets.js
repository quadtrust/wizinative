import React, { Component } from 'react';
import {
  AsyncStorage
} from 'react-native';
import Socket from 'react-native-socketio';
import API from './API';

let socket = undefined;
// const base_url = "http://192.168.0.101:8080/";
const base_url = "http://meteoricvisions.com/";

export default class Sockets extends Component {

  static connect() {
    socket = new Socket(base_url);
    socket.connect();

    socket.on('connect', () => {
      API.getToken((token) => {
        AsyncStorage.getItem('user').then((sUser) => {
            socket.emit('authentication', {
                token: token,
                avatar: JSON.parse(sUser).user.image
            });
        });
      });
    });
  }

  static emit(event, data) {
    socket.emit(event, data);
  }

  static on(event, cb) {
    socket.on(event, cb);
  }

  static off(event) {
    socket.off(event);
  }

}
