import React, {Component} from 'react'
import {
    View,
    Text,
    Image
} from 'react-native'
import styles from './Style'
import Header from  './Header'
import Button from 'react-native-button'

export default class Lives extends Component {
    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} player={true}/>
           <View style={[styles.bodyWidth,styles.whiteBack,{alignItems:'center',justifyContent:'center'}]}>
              <View>
                  <Text style={{fontWeight:'700', fontSize:23,color:'black'}}>
                      YOU HAVE RUN OUT OF LIVES
                  </Text>
              </View>
               <View style={{alignItems:'center'}}>
                   <Image style={{height:90,width:100,resizeMode:'contain',margin:40}} source={require('../assets/images/recharge/heart.png')}/>
               </View>
                <View>
                    <Button
                        containerStyle={[{
                            marginTop: 20,
                            padding:5,
                            width:230,
                            borderRadius:5,
                            backgroundColor:'#1EBBA7',
                        }]}
                        style={[{fontSize: 18, color: 'white'}]}
                    >KD 1.00 RECHARGE NOW</Button>
                </View>
           </View>
            </View>
        );
    }
}