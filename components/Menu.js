import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet
} from 'react-native'
import language from './Language'

export default class Menu extends Component {

  componentDidMount() {
  }

  render() {
    return (
      <View style={[styles.fullWidth]}>
        {/*background*/}
        <Image style={[styles.fullWidth, styles.zoomed, styles.notKnown]}
          source={require('../assets/images/menu/back.png')}>

          <View style={[styles.fullWidth, styles.blackColor, styles.row, { justifyContent: 'space-between' }]}>
            <TouchableOpacity onPress={() => this.props.doPop()}>
              <Image style={[styles.contain, { height: 35, marginTop: 10, width: 50 }]}
                source={require('../assets/images/common/back.png')} />
            </TouchableOpacity>
            <Text style={[styles.menuText, styles.textCenter, { marginRight: 45 }]}>{language.menu}</Text>
            <Text>

            </Text>
          </View>

          <View style={[styles.center, styles.transparenWhite, { flex: 2, marginBottom: 10 }]}>
            <Image source={require('../assets/images/menu/menuCopy.png')}
              style={[{ resizeMode: 'contain', width: 350, height: 100 }]} />
            <TouchableOpacity onPress={() => this.props.doReplace('profile')}>
              <Image source={require('../assets/images/menu/profile.png')}
                style={[{ resizeMode: 'contain', width: 200, height: 22 }]} />
            </TouchableOpacity>
          </View>

          <View style={[{ flex: 7, justifyContent: 'center', marginBottom: 10 }]}>

            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.doResetTo('newGame')}>
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> {language.home} </Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.doReplace('result')}>
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> {language.report} </Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.doReplace('statistics')}>
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> {language.statistic} </Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.doReplace('question')}>
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> {language.bank} </Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.doReplace('social')}>
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> {language.media} </Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.doReplace('shop')}>
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> {language.shop} </Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.doReplace('setting')}>
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> {language.settings} </Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.doReplace('help')}>
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> {language.help} </Text>
              </Image>
            </TouchableOpacity>

          </View>
        </Image>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  fullWidth: {
    flex: 1
  },
  menuContainer: { height: 55, width: 200 },
  menuContainerText: { fontWeight: '800', fontSize: 16, color: '#fff', padding: 10 },
  contain: {
    resizeMode: 'contain'
  },
  transparenWhite: {
    backgroundColor: 'rgba(255,255,255,0.6)',
    paddingBottom: 10

  },
  notKnown: {
    height: undefined,
    width: undefined
  },
  zoomed: {
    resizeMode: 'cover'
  },
  row: {
    flexDirection: 'row'
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  textCenter: {
    textAlign: 'center'
  },
  menuText: {
    color: '#fff',
    fontWeight: '800',
    fontSize: 40
  },
  height: {
    height: 30
  },
  blackColor: {
    backgroundColor: '#403c3d'
  },

});