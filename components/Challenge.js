import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    AsyncStorage,
    ToastAndroid,
    NativeModules
} from 'react-native'
import styles from './Style'
import Header from './Header'
import Button from 'react-native-button'
import Sockets from './Sockets'
import API from './API'

export default class Challenge extends Component {
    constructor(props) {
        super(props);
        this.state = {
            challenger1: require('../assets/human/human1.png'),
            challenger2: require('../assets/human/human2.png'),
            user1: '',
            user2: '',
            buttonText: 'Waiting...',
            playerJoined: false,
            user2_id: undefined,
            random: false,
            spin: false
        }
    }

    componentDidMount() {
        if (this.props.data && this.props.data.user2) {
            API.getIdByUsername(this.props.data.user2, this.props.doResetTo, (response) => {
                if (response.user) {
                    this.setState({
                        user2: this.props.data.user2,
                        user2_id: response.user.id,
                        challenger2: API.map_avatar(this.props.data.avatar)
                    });
                    this.props.inGame(this.props.data.user2);
                }
            });
        } else if (this.props.data && this.props.data.random) {
            this.setState({
                random: true
            });
            Sockets.on('wait-random', () => {
                ToastAndroid.show('Waiting for a player', ToastAndroid.SHORT);
            });
            Sockets.on('match-random', (username) => {
                Sockets.emit('send-invite', { username: username[0].username });
                ToastAndroid.show('Player found, waiting to accept', ToastAndroid.SHORT);
            });
            Sockets.on('spin', () => {
                this.setState({
                    spin: true,
                    buttonText: 'Spin',
                    playerJoined: true
                });
            });
            Sockets.emit('add-random', {});
        }
        Sockets.on('game-start', (data) => {
            AsyncStorage.setItem('theme', data[0].theme).then(() => {
                API.getQuestions(data[0].GameId, this.props.doResetTo, (response) => {
                    AsyncStorage.setItem('question', JSON.stringify(response)).then(() => {
                        this.props.inGame(this.state.user2);
                        this.props.doResetTo('subjectVersus', { user2_username: this.state.user2 });
                    });
                });
            });
        });
        Sockets.on('challenge-answer', (answer) => {
            if (answer[0].answer) {
                API.getIdByUsername(answer[0].username, this.props.doResetTo, (response) => {
                    if (response.user) {
                        this.setState({
                            user2: answer[0].username,
                            user2_id: response.user.id,
                            challenger2: API.map_avatar(answer[0].avatar)
                        });
                        if (!this.state.random) {
                            this.setState({
                                playerJoined: true,
                                buttonText: 'Spin',
                                spin: true
                            });
                        }
                        ToastAndroid.show('User accepted your challenge', ToastAndroid.SHORT);
                        this.props.inGame(answer[0].username);
                    }
                });
            } else {
                ToastAndroid.show('User declined your challenge', ToastAndroid.SHORT);
                this.props.doPop();
            }
        });
        AsyncStorage.getItem('user', (error, sUser) => {
            let user = JSON.parse(sUser).user;
            this.setState({
                user1: user.username,
                challenger1: API.map_avatar(user.image)
            });
        });
    }

    componentWillUnmount() {
        NativeModules.SocketIO.off('challenge-answer');
        NativeModules.SocketIO.off('wait-random');
        NativeModules.SocketIO.off('match-random');
        Sockets.emit('remove-random', {});
    }

    startGame() {
        if (this.state.spin) {
            this.props.doResetTo('randomSpinner', { user2_id: this.state.user2_id });
        } else {
            API.start_game(1, this.props.doResetTo, (resp) => {
                if (resp.error) {
                    ToastAndroid.show('Please spin again!', ToastAndroid.SHORT);
                } else {
                    AsyncStorage.setItem('question', JSON.stringify(resp)).then(() => {
                        this.props.doResetTo('subjectVersus', { user2_username: this.state.user2 });
                    });
                }
            }, this.state.user2_id);
        }
    }

    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true} />
                <View style={[styles.bodyWidth, styles.backBlack]}>
                    <Text style={[styles.challenge_header, styles.center, { marginTop: 25 }]}>ARE YOU READY?</Text>
                    <Text style={[styles.challenge_sub_header, styles.center, { marginTop: 25 }]}>MEET YOUR CHALLENGER</Text>
                    <View style={[styles.row, { justifyContent: 'space-around', marginTop: 20 }]}>
                        <View style={[styles.center_element]}>
                            <Image style={[styles.contain, { height: 200, width: 250 }]} source={this.state.challenger1} />
                            <Text style={{ color: 'white' }}>{this.state.user1}</Text>
                        </View>
                        <Text style={{ color: '#FD9D00', fontSize: 28, fontWeight: '900', textAlignVertical: 'center' }}>
                            VS
                    </Text>
                        <View style={[styles.center_element]}>
                            <Image style={[styles.contain, { height: 200, width: 250 }]} source={this.state.challenger2} />
                            <Text style={{ color: 'white' }}>{this.state.user2}</Text>
                        </View>
                    </View>
                    <Button
                        containerStyle={[styles.challengeButton, styles.green, {
                            marginTop: 20,
                            marginLeft: 35,
                            marginRight: 35
                        }]}
                        style={[{ fontSize: 18, color: 'white' }]}
                        disabled={!this.state.playerJoined}
                        onPress={() => this.startGame()}
                    >
                        {this.state.buttonText}
                    </Button>
                </View>
            </View>
        );
    }
}