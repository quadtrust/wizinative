import React, { Component } from 'react';
import {
  Navigator,
  BackAndroid,
  ToastAndroid,
  View,
  Modal,
  Text,
  TouchableHighlight,
  NativeModules,
  Image
} from 'react-native';
import Scene from './Scene';
import language from './Language';
import Sounds from './Sounds';
import Sockets from './Sockets';

let shouldExit = false;
let _navigator;
let initialRoute = 'login';
let inviteAcceptCallback = () => { };
let inviteDeclineCallback = () => { };
let username = "";

BackAndroid.addEventListener('hardwareBackPress', () => {
  if (_navigator.getCurrentRoutes().length > 1) {
    Sounds.playBack();
    _navigator.pop();
  } else if (!shouldExit) {
    ToastAndroid.show('Press back again to exit', ToastAndroid.SHORT);
    shouldExit = true;
    setTimeout(() => {
      shouldExit = false;
    }, 2000);
  } else {
    Sounds.stopBackground();
    return false;
  }
  return true;
});

export default class Loader extends Component {
  // componentDidMount(){
  //     AsyncStorage.getItem('user').then((resp) =>{
  //         if(resp !== null ||  resp !== ''){
  //             initialRoute = 'newGame';
  //             _navigator.resetTo({name: 'newGame', index: 0});
  //         }
  //     })
  // }
  constructor(props) {
    super(props);

    this.state = {
      modal: false,
      inGame: false,
      opponent: '',
      lives_left: 10,
      spins_left: 10,
      loading: true
    }
  }

  componentDidMount() {
    language.setLanguage('en');
  }

  checkUserInGame(name) {
    if (this.state.inGame) {
      if (name != 'menu' && name != 'randomSpinner' && name != 'subjectVersus') {
        Sockets.emit('user-left', { username: this.state.opponent });
        this.setState({ inGame: false, opponent: '' });
        NativeModules.SocketIO.off('user-left');
      }
    }
  }

  render() {
    let loadingOpacity = this.state.loading ? 1 : 0;
    let loadingZIndex = this.state.loading ? 1 : -10;
    return (
      <View style={{ flex: 1 }}>
        <Navigator
          ref={(navigator) => {
                        _navigator = navigator
                    }}
          initialRoute={{ name: initialRoute, index: 0 }}
          renderScene={(route, navigator) => {
                        return <Scene
                            name={route.name}
                            data={route.data}
                            doLoad={(name, data) => {
                                this.checkUserInGame(name);
                                const nextIndex = route.index + 1;
                                navigator.push({
                                    name: name,
                                    index: nextIndex,
                                    data: data
                                });
                            }}
                            doReplace={(name, data) => {
                                if (this.state.inGame) {
                                    navigator.pop();
                                }
                                navigator.replace({
                                    name: name,
                                    index: route.index,
                                    data: data
                                });
                                this.checkUserInGame(name);
                            }}
                            doResetTo={(name, data) => {
                                this.checkUserInGame(name);
                                navigator.resetTo({
                                    name: name,
                                    index: 0,
                                    data: data
                                });
                            }}
                            doPop={() => {
                                Sounds.playBack();
                                let routes = navigator.getCurrentRoutes();
                                if (routes.length == 1) {
                                    this.checkUserInGame('newGame');
                                    navigator.resetTo({ name: 'newGame', index: 0 });
                                } else {
                                    this.checkUserInGame(routes[routes.length - 2].name);
                                    navigator.pop();
                                }
                            }}
                            inviteShow={(username, callbackAccept, callbackDecline) => {
                                this.username = username;
                                this.inviteAcceptCallback = callbackAccept;
                                this.inviteDeclineCallback = callbackDecline;
                                this.setState({ modal: true });
                            }}
                            connectSocket={() => {
                                Sockets.connect();
                            }}
                            inGame={(opp) => {
                                if (!this.state.inGame) {
                                    this.setState({ opponent: opp });
                                    this.setState({ inGame: true });
                                    Sockets.on('user-left', () => {
                                        if (this.state.inGame) {
                                            ToastAndroid.show('Opponent left the game', ToastAndroid.SHORT);
                                            navigator.resetTo({ name: 'newGame', index: 0 });
                                            this.setState({ inGame: false });
                                        }
                                    });
                                }
                            }}
                            gameOver={() => {
                                if (this.state.inGame) {
                                    this.setState({ inGame: false });
                                    NativeModules.SocketIO.off('user-left');
                                }
                            }}
                            lives={this.state.lives_left}
                            spins={this.state.spins_left}
                            reduceLife={() => {this.setState({lives_left: this.state.lives_left - 1})}}
                            reduceSpin={() => {this.setState({spins_left: this.state.spins_left - 1})}}
                            loading={() => this.setState({ loading: true })}
                            loaded={() => this.setState({ loading: false })}
                        />
                    }}
        />
        {this.state.modal ? <Modal
            animationType={"slide"}
            transparent={true}
            visible={this.state.modal}
            onRequestClose={() => { this.inviteDeclineCallback(); this.setState({ modal: false }); }}
          >
            <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center', padding: 20 }}>
              <View>
                <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'white' }}>{this.username} has challenged you for a duel. Would you like to accept?</Text>
              </View>
              <View style={{ flexDirection: 'row', paddingTop: 20 }}>
                <TouchableHighlight onPress={() => { this.inviteAcceptCallback(); this.setState({ modal: false }); }} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', height: 30, backgroundColor: 'green' }}><Text>Yes</Text></TouchableHighlight>
                <TouchableHighlight onPress={() => { this.setState({ modal: false }); this.inviteDeclineCallback(); }} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', height: 30, backgroundColor: 'red' }}><Text>No</Text></TouchableHighlight>
              </View>
            </View>
          </Modal> : null}

          <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center', padding: 20, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, opacity: loadingOpacity, zIndex: loadingZIndex }}>
            <View style={{ flex: 1 }} />
            <Image source={require('../assets/images/logo.gif')}
                   style={{ flex: 1, height: undefined, width: undefined, resizeMode: 'contain' }} />
            <Text style={{ color: 'white', fontSize: 18, textAlign: 'center' }}>Loading...</Text>
            <View style={{ flex: 1 }} />
          </View>
      </View>
    )
  }
}