import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    AsyncStorage,
    TouchableOpacity,
    ListView,
    ToastAndroid
} from 'react-native';
import styles from './Style';
import Header from './Header';
import GridView from 'react-native-easy-gridview';
import API from './API';
import Sockets from './Sockets';

export default class ChooseCharacter extends Component {
    constructor(props) {
        super(props);
        let ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.state = {
            avatars: ds.cloneWithRows([]),
            unlocked: [],
            myAvatar: null
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('user').then((sUser) => {
            let user = JSON.parse(sUser);
            this.setState({
                myAvatar: user.user.image
            });
        });
        API.getUnlockedAvatars(this.props.doResetTo, (response) => {
            if(response.error) {
                ToastAndroid.show('Network error', ToastAndroid.SHORT);
                this.props.doPop();
            } else {
                let data = Array.apply(null, {length: 50}).map(Number.call, Number);
                this.setState({
                    unlocked: response.avatars,
                    avatars: this.state.avatars.cloneWithRows(data)
                });
            }
        });
    }

    _chooseChar(char) {
        API.update_avatar(char, this.props.doResetTo, (resp) => {
            if(resp.error) {
                ToastAndroid.show('Network error, please try again', ToastAndroid.SHORT);
            } else {
                this.setState({
                    myAvatar: char,
                    avatars: this.state.avatars.cloneWithRows([])
                });
                AsyncStorage.getItem('user').then((sUser) => {
                    let user = JSON.parse(sUser);
                    user.user.image = char;
                    Sockets.emit('avatar-change', {avatar: char});
                    AsyncStorage.setItem('user', JSON.stringify(user)).then(() => {
                        let data = Array.apply(null, {length: 50}).map(Number.call, Number);

                        this.setState({
                            avatars: this.state.avatars.cloneWithRows(data)
                        });
                        AsyncStorage.setItem('character', JSON.stringify(char)).then(() => {
                                this.props.doLoad('characterSelected');
                            }
                        );
                    });
                });
            }
        });
    }

    avatarRow(a) {
        return <View style={{margin: 5}}>
            <TouchableOpacity
                disabled={(this.state.unlocked !== false ? (this.state.unlocked.indexOf(a + 1) <= -1) : true)}
                onPress={() => this._chooseChar(a+1)}
                style={this.state.myAvatar == (a + 1) ? [inlineStyles.myAvatar] : []}
            >
                <Image
                    source={API.map_avatar(a + 1)}
                    style={{
                        flex: 1,
                        height: 100,
                        width: undefined,
                        opacity: (this.state.unlocked !== false ? (this.state.unlocked.indexOf(a+1) > -1 ? 1 : 0.2) : 0.2)
                    }}
                    resizeMode={'contain'}
                />
            </TouchableOpacity>
        </View>
    }

    render() {
        return (
            <View style={styles.fullWidth}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true}/>
                <View style={[styles.bodyWidth, styles.backBlack]}>
                    <Text style={[styles.avatar_header, styles.center]}>CHOOSE YOUR AVATAR</Text>
                    <GridView
                        dataSource={this.state.avatars}
                        renderRow={(item) => this.avatarRow(item)}
                        numberOfItemsPerRow={4}
                        removeClippedSubviews={false}
                        initialListSize={1}
                        pageSize={4}
                        contentContainerStyle={{paddingBottom: 10}}
                    />
                </View>
            </View>
        );
    }
}

const inlineStyles = {
    myAvatar: {
        backgroundColor: "rgba(255, 255, 255, 0.3)",
        borderRadius: 5,
        padding: 5
    }
};