import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import Header from './Header';
import API from './API';

const style = {
    heading: {
        fontSize: 18,
        color: '#c41220',
        fontWeight: 'bold'
    },
    listRight: {
        fontSize: 18,
        color: '#00aeff',
        fontWeight: 'bold',
        flex: 1,
        textAlign: 'right'
    },
    listLeft: {
        fontSize: 18,
        color: '#00aeff',
        fontWeight: 'bold',
        flex: 1,
        textAlign: 'left'
    },
    borderBottom: {
        borderBottomWidth: 1,
        borderBottomColor: 'black'
    }
};

export default class Directorates extends Component {
    constructor(props) {
        super(props);

        this.state = {
            directorates: []
        }
    }

    componentDidMount() {
        API.getDirectorates((directorates) => {
            if(directorates.error) {

            } else {
                this.setState({
                    directorates: directorates.directorates
                });
            }
        });
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: 'white'}}>
                <Header style={{flex: 1}} doLoad={this.props.doLoad} doPop={this.props.doPop} />
                <View style={{flex: 18, padding: 10}}>
                    <Text
                        style={{color: '#00aeff', fontSize: 30, fontWeight: 'bold', textAlign: 'center'}}
                    >COMPETITION</Text>
                    <Image
                        style={{flex: 1, height: undefined, width: undefined}}
                        resizeMode='contain'
                        source={require('../assets/images/directorates/map.png')} />

                    <View>
                        <View style={[{flexDirection: 'row', justifyContent: 'space-between'}, style.borderBottom]}>
                            <Text style={style.heading}>GOVERNORATE</Text>
                            <Text style={style.heading}>PLAYERS</Text>
                            <Text style={style.heading}>SCORES</Text>
                        </View>

                        {this.state.directorates.map((directorate, index) => {
                            return (
                                <TouchableOpacity onPress={() => {
                                    this.props.doLoad('topDirectoratePlayers', { id: directorate.id })
                                }} key={directorate.id} style={[{flexDirection: 'row', justifyContent: 'space-between'}, style.borderBottom]}>
                                    <Text style={style.listLeft}>{directorate.name}</Text>
                                    <Text style={style.listRight}>{directorate.user_count}</Text>
                                    <Text style={style.listRight}>{directorate.total_marks}</Text>
                                </TouchableOpacity>
                            );
                        })}

                    </View>
                </View>
            </View>
        )
    }

}