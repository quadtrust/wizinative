import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    ToastAndroid,
    StyleSheet,
    AsyncStorage,
    TouchableOpacity
} from 'react-native'
import Header from './Header'
import PlayerStats from './PlayerStats'
import API from './API'
import language from './Language'

let subject_id = '';

export default class SubjectSelection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSocial: false,
            isScience: false,
            isMaths: false,
            isArabic: false,
            isEnglish: false,
            isIslamic: false
        }
    }

    _startGame(id) {
        switch (id) {
            case 1:
                AsyncStorage.setItem('theme', 'english');
                break;
            case 2:
                AsyncStorage.setItem('theme', 'maths');
                break;
            case 3:
                AsyncStorage.setItem('theme', 'islam');
                break;
            case 4:
                AsyncStorage.setItem('theme', 'arabic');
                break;
            case 5:
                AsyncStorage.setItem('theme', 'science');
                break;
            case 6:
                AsyncStorage.setItem('theme', 'social');
                break;
            default:
                ToastAndroid.show('Please select a subject first', ToastAndroid.SHORT);
        }

        API.start_game(id, this.props.doResetTo, (resp) => {
            if (resp.error) {
                ToastAndroid.show('Please select a different subject', ToastAndroid.SHORT);
            } else {
                AsyncStorage.setItem('question', JSON.stringify(resp)).then(() => {
                    this.props.doResetTo('subject');
                });

            }
        })


    }

    _selectedSubject(subject) {

        switch (subject) {
            case 'social':
                this.state.isSocial ? subject_id = '' : subject_id = 6;
                this.setState({ isSocial: !this.state.isSocial });
                this.setState({ isScience: false });
                this.setState({ isMaths: false });
                this.setState({ isArabic: false });
                this.setState({ isEnglish: false });
                this.setState({ isIslamic: false });

                break;

            case 'science':
                this.state.isScience ? subject_id = '' : subject_id = 5;
                this.setState({ isScience: !this.state.isScience });
                this.setState({ isSocial: false });
                this.setState({ isMaths: false });
                this.setState({ isArabic: false });
                this.setState({ isEnglish: false });
                this.setState({ isIslamic: false });

                break;

            case 'maths':
                this.state.isMaths ? subject_id = '' : subject_id = 2;
                this.setState({ isMaths: !this.state.isMaths });
                this.setState({ isSocial: false });
                this.setState({ isArabic: false });
                this.setState({ isEnglish: false });
                this.setState({ isIslamic: false });
                this.setState({ isScience: false });


                break;

            case 'arabic':
                this.state.isArabic ? subject_id = '' : subject_id = 4;
                this.setState({ isArabic: !this.state.isArabic });
                this.setState({ isMaths: false });
                this.setState({ isSocial: false });
                this.setState({ isEnglish: false });
                this.setState({ isIslamic: false });
                this.setState({ isScience: false });

                break;

            case 'english':
                this.state.isEnglish ? subject_id = '' : subject_id = 1;
                this.setState({ isEnglish: !this.state.isEnglish });
                this.setState({ isArabic: false });
                this.setState({ isMaths: false });
                this.setState({ isSocial: false });
                this.setState({ isIslamic: false });
                this.setState({ isScience: false });


                break;

            case 'islam':
                this.state.isIslamic ? subject_id = '' : subject_id = 3;
                this.setState({ isIslamic: !this.state.isIslamic });
                this.setState({ isArabic: false });
                this.setState({ isMaths: false });
                this.setState({ isSocial: false });
                this.setState({ isEnglish: false });
                this.setState({ isScience: false });

                break;
            default:
                subject_id = '';
                break;
        }
    }


    render() {

        return (
            <View style={styles.fullWidth}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true} />
                <PlayerStats />
                <View style={[{ flex: 18, justifyContent: 'center', marginBottom: 10 }]}>

                    <Text style={[styles.textCenter, styles.heading]}> {language.select_subject}</Text>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => this._selectedSubject('social')}
                            style={[this.state.isSocial ? styles.activeBack : styles.notActive, {
                                flex: 1,
                                padding: 5
                            }]}>
                            <Image style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                                resizeMode='contain'
                                source={require('../assets/images/subject/socia.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this._selectedSubject('science')}
                            style={[this.state.isScience ? styles.activeBack : styles.notActive, {
                                flex: 1,
                                padding: 5
                            }]}>
                            <Image style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                                resizeMode='contain'
                                source={require('../assets/images/subject/science.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => this._selectedSubject('maths')}
                            style={[this.state.isMaths ? styles.activeBack : styles.notActive, {
                                flex: 1,
                                padding: 5
                            }]}>
                            <Image style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                                resizeMode='contain'
                                source={require('../assets/images/subject/maths.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this._selectedSubject('arabic')}
                            style={[this.state.isArabic ? styles.activeBack : styles.notActive, {
                                flex: 1,
                                padding: 5
                            }]}>
                            <Image style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                                resizeMode='contain'
                                source={require('../assets/images/subject/arabic.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => this._selectedSubject('english')}
                            style={[this.state.isEnglish ? styles.activeBack : styles.notActive, {
                                flex: 1,
                                padding: 5
                            }]}>
                            <Image style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                                resizeMode='contain'
                                source={require('../assets/images/subject/english.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this._selectedSubject('islam')}
                            style={[this.state.isIslamic ? styles.activeBack : styles.notActive, {
                                flex: 1,
                                padding: 5
                            }]}>
                            <Image style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                                resizeMode='contain'
                                source={require('../assets/images/subject/islam.png')} />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => this._startGame(subject_id)}
                        style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image style={[styles.contain, { height: 35, width: 120, marginTop: 10 }]}
                            source={require('../assets/images/subject/start.png')} />
                    </TouchableOpacity>
                </View>


            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#2B292A'
    },
    activeBack: {
        backgroundColor: "grey",
        borderRadius: 20
    },
    notActive: {
        backgroundColor: "transparent"
    },
    heading: {
        fontSize: 25,
        fontWeight: '800',
        color: 'orange'
    },
    subIcon: { height: 130, width: 130, margin: 8 },
    textShadow: {
        textShadowColor: 'black',
        textShadowOffset: { width: 5, height: 5 },
        textShadowRadius: 15
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCenter: {
        textAlign: 'center'
    },
    bodyWidth: {
        flex: 18
    },
    whiteBackground: {
        backgroundColor: "white"
    },

    zoomed: {
        resizeMode: 'cover'
    },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
});
