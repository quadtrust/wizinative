import React, { Component } from 'react'
import {
    ToastAndroid
} from 'react-native'
const baseURL = "http://meteoricvisions.com/";
// const baseURL = "http://192.168.0.100:3003/";

let jsonHeaders = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
let JSONHeaders = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};

let dataStore = {};

export default class API extends Component {

    static getToken(cb) {
        cb(dataStore['token']);
    }

    static handleResponse(response, cb, reset) {
        try {
            let result = JSON.parse(response._bodyText);
            if (result.unauthorized == true) {
                reset('login');
                ToastAndroid.show('You logged in from other device', ToastAndroid.SHORT);
            } else {
                cb(result);
            }
        } catch (error) {
            cb({ error: error })
        }
    }

    static login(email, password, cb) {
        fetch(baseURL + 'v1/users/login', {
            method: 'POST',
            headers: JSONHeaders,
            body: JSON.stringify({
                user: {
                    email: email,
                    password: password
                }
            })
        }).then((response) => {
            try {
                let user = JSON.parse(response._bodyText);
                if (user.user) {
                    dataStore['token'] = user.user.token;
                    jsonHeaders['Authorization'] = 'Bearer ' + user.user.token;
                    cb(user);
                } else {
                    cb(user);
                }
            } catch (error) {
                cb({ error: error });
            }
        }).catch((err) => {
            cb({error: true})
        })
    }

    static setToken(token) {
        dataStore['token'] = token;
        jsonHeaders['Authorization'] = 'Bearer ' + token;
    }

    static register(first_name, last_name, school, dob, email, username, age, language, gender, class_id, password, password_confirm, directorate, cb) {
        fetch(baseURL + 'v1/users', {
            method: 'POST',
            headers: JSONHeaders,
            body: JSON.stringify({
                user: {
                    first_name: first_name,
                    last_name: last_name,
                    school: school,
                    dob: dob,
                    email: email,
                    username: username,
                    age: age,
                    language: language,
                    gender: gender,
                    ClassId: class_id,
                    password: password,
                    password_confirm: password_confirm,
                    DirectorateId: directorate
                }
            })
        }).then((response) => {
            try {
                let user = JSON.parse(response._bodyText);
                if (user.user) {
                    dataStore['token'] = user.user.token;
                    jsonHeaders['Authorization'] = 'Bearer ' + user.user.token;
                    cb(user)
                } else if (user.errors) {
                    cb({ error: true, message: user.errors.errors[0].message })
                }
            } catch (error) {
                cb({ error: error })
            }
        })
    }

    static getCategories(reset, cb) {
        fetch(baseURL + 'v1/categories', {
            method: 'GET'
        }).then((response) => {
            this.handleResponse(response, cb, reset)
        })
    }

    static get_standards(reset, cb) {
        fetch(baseURL + 'v1/standards', {
            method: 'GET',
            headers: jsonHeaders
        }).then((response) => {
            this.handleResponse(response, cb, reset)

        });
    }

    static getResult(reset, cb) {
        fetch(baseURL + 'v1/results', {
            method: 'GET',
            headers: jsonHeaders
        }).then((response) => {
            this.handleResponse(response, cb, reset)

        });
    }

    static nextQuestion(question_id, data, reset, cb) {
        fetch(baseURL + 'v1/answers/' + question_id, {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                answer: data
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset)
        }).catch((err) => {
            cb({ error: err })
        })
    }

    static get_statistics(reset, cb) {
        fetch(baseURL + 'v1/games/statistics', {
            method: 'GET',
            headers: jsonHeaders
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        })
    }

    static sendInvite(data, reset, cb) {
        fetch(baseURL + 'v1/invites', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                invite: data
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        })
    }

    static end_game(reset, data, cb) {
        fetch(baseURL + 'v1/answers/end-game', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                game: { GameQuestionId: data }
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        });
    }

    static changeLanguage(language, reset, cb) {
        fetch(baseURL + 'v1/users?_method=PATCH', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                user: {
                    language: language
                }
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset)
        })
    }

    static get_me(reset, cb) {
        fetch(baseURL + 'v1/users/me', {
            method: 'GET',
            headers: jsonHeaders
        }).then((response) => {
            this.handleResponse(response, cb, reset)
        }).catch((err) => {
            cb({ error: err })
        })
    }

    static update_user(data, reset, cb) {
        fetch(baseURL + 'v1/users/update?_method=PATCH', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                user: data
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        });
    }

    static update_avatar(image, reset, cb) {
        fetch(baseURL + 'v1/users/update-avatar?_method=PATCH', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                user: {
                    image: image
                }
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        });
    }

    static updateSetting(data, reset, cb) {
        fetch(baseURL + 'v1/settings?_method=PATCH', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                setting: data
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset)
        })
    }

    static start_game(subject_id, reset, cb, otherUserId = null) {
        fetch(baseURL + 'v1/games', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                game: {
                    CategoryId: subject_id,
                    OtherUserId: otherUserId
                }
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        }).catch((err) => {
            cb({ error: err });
        });
    }

    static getIdByUsername(username, reset, cb) {
        fetch(baseURL + 'v1/users/get-id', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({ user: { username: username } })
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        }).catch((err) => {
            cb({ error: err });
        });
    }

    static getUsernameById(id, reset, cb) {
        fetch(baseURL + 'v1/users/get-username', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({ user: { id: id } })
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        }).catch((error) => {
            cb({ error: true });
        });
    }

    static getQuestions(game_id, reset, cb) {
        fetch(baseURL + 'v1/games/questions/' + game_id, {
            headers: jsonHeaders
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        }).catch((err) => {
            cb({ error: true });
        });
    }

    static getDirectorates(cb) {
        fetch(baseURL + 'v1/directorates').then((response) => {
            this.handleResponse(response, cb, () => {})
        }).catch((err) => {
            cb({error: true});
        });
    }

    static getTopDirectoratePlayers(id, reset, cb) {
        fetch(baseURL + 'v1/directorates/' + id, {
            headers: jsonHeaders
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        });
    }

    static getUnlockedAvatars(reset, cb) {
        fetch(baseURL + 'v1/users/unlocked-avatars', {
            headers: jsonHeaders
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        })
    }

    static unlockAvatar(avatar, reset, cb) {
        fetch(baseURL + 'v1/users/unlock-avatar', {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                avatar: avatar
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        }).catch((error) => {
            cb({error: true});
        });
    }

    static suggestQuestion(data, reset, cb) {
        fetch(baseURL + 'v1/questions', {
            method: "POST",
            headers: jsonHeaders,
            body: JSON.stringify({
                question: data
            })
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        }).catch((error) => {
            cb({error: true});
        });
    }

    static checkQuestionStatus(reset, cb) {
        fetch(baseURL + 'v1/questions/status', {
            headers: jsonHeaders
        }).then((response) => {
            this.handleResponse(response, cb, reset);
        }).catch((error) => {
            cb({ error: true });
        });
    }

    static map_avatar(avatar) {
        let mapper = {
            1: require('../assets/human/human1.png'),
            2: require('../assets/monster/monster1.png'),
            3: require('../assets/human/human2.png'),
            4: require('../assets/monster/monster2.png'),
            5: require('../assets/human/human3.png'),
            6: require('../assets/monster/monster3.png'),
            7: require('../assets/human/human4.png'),
            8: require('../assets/monster/monster4.png'),
            9: require('../assets/human/human5.png'),
            10: require('../assets/monster/monster5.png'),
            11: require('../assets/human/human6.png'),
            12: require('../assets/monster/monster6.png'),
            13: require('../assets/human/human7.png'),
            14: require('../assets/monster/monster7.png'),
            15: require('../assets/human/human8.png'),
            16: require('../assets/monster/monster8.png'),
            17: require('../assets/human/human9.png'),
            18: require('../assets/monster/monster9.png'),
            19: require('../assets/human/human10.png'),
            20: require('../assets/monster/monster10.png'),
            21: require('../assets/human/human11.png'),
            22: require('../assets/monster/monster11.png'),
            23: require('../assets/human/human12.png'),
            24: require('../assets/monster/monster12.png'),
            25: require('../assets/monster/monster13.png'),
            26: require('../assets/monster/monster14.png'),
            27: require('../assets/monster/monster15.png'),
            28: require('../assets/monster/monster16.png'),
            29: require('../assets/monster/monster17.png'),
            30: require('../assets/monster/monster18.png'),
            31: require('../assets/monster/monster19.png'),
            32: require('../assets/monster/monster20.png'),
            33: require('../assets/monster/monster21.png'),
            34: require('../assets/monster/monster22.png'),
            35: require('../assets/monster/monster23.png'),
            36: require('../assets/monster/monster24.png'),
            37: require('../assets/monster/monster25.png'),
            38: require('../assets/monster/monster26.png'),
            39: require('../assets/monster/monster27.png'),
            40: require('../assets/monster/monster28.png'),
            41: require('../assets/monster/monster29.png'),
            42: require('../assets/monster/monster30.png'),
            43: require('../assets/monster/monster31.png'),
            44: require('../assets/monster/monster32.png'),
            45: require('../assets/monster/monster33.png'),
            46: require('../assets/monster/monster34.png'),
            47: require('../assets/monster/monster35.png'),
            48: require('../assets/monster/monster36.png'),
            49: require('../assets/monster/monster37.png'),
            50: require('../assets/monster/monster38.png')
        };
        return mapper[avatar];
    }

}
