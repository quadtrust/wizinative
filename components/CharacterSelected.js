import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    AsyncStorage
} from 'react-native';
import styles from './Style';
import Header from './Header';
import API from './API';

export default class CharacterSelected extends Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar: require('../assets/human/human1.png'),
            user: 'LUCIFER'
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('user').then((user) => {
            user = JSON.parse(user);
            this.setState({
                user: user.user.username.toUpperCase()
            });
        });
        AsyncStorage.getItem('character').then((char) => {
            API.update_avatar(char, this.props.doResetTo, (response) => {});
            switch (JSON.parse(char)) {
                case 1:
                    this.setState({avatar: require('../assets/human/human1.png')});
                    break;
                case 2:
                    this.setState({avatar: require('../assets/monster/monster1.png')});
                    break;
                case 3:
                    this.setState({avatar: require('../assets/human/human2.png')});
                    break;
                case 4:
                    this.setState({avatar: require('../assets/monster/monster2.png')});
                    break;
                case 5:
                    this.setState({avatar: require('../assets/human/human3.png')});
                    break;
                case 6:
                    this.setState({avatar: require('../assets/monster/monster3.png')});
                    break;
                case 7:
                    this.setState({avatar: require('../assets/human/human4.png')});
                    break;
                case 8:
                    this.setState({avatar: require('../assets/monster/monster4.png')});
                    break;
                case 9:
                    this.setState({avatar: require('../assets/human/human5.png')});
                    break;
                case 10:
                    this.setState({avatar: require('../assets/monster/monster5.png')});
                    break;
                case 11 :
                    this.setState({avatar: require('../assets/human/human6.png')});
                    break;
                case 12:
                    this.setState({avatar: require('../assets/monster/monster6.png')});
                    break;
                case 13 :
                    this.setState({avatar: require('../assets/human/human7.png')});
                    break;
                case 14:
                    this.setState({avatar: require('../assets/monster/monster7.png')});
                    break;
                case 15:
                    this.setState({avatar: require('../assets/human/human8.png')});
                    break;
                case 16 :
                    this.setState({avatar: require('../assets/monster/monster8.png')});
                    break;
                case  17:
                    this.setState({avatar: require('../assets/human/human9.png')});
                    break;
                case 18:
                    this.setState({avatar: require('../assets/monster/monster9.png')});
                    break;
                case 19 :
                    this.setState({avatar: require('../assets/human/human10.png')});
                    break;
                case 20:
                    this.setState({avatar: require('../assets/monster/monster10.png')});
                    break;
                case 21:
                    this.setState({avatar: require('../assets/human/human11.png')});
                    break;
                case 22:
                    this.setState({avatar: require('../assets/monster/monster11.png')});
                    break;
                case 23 :
                    this.setState({avatar: require('../assets/human/human12.png')});
                    break;
                case 24:
                    this.setState({avatar: require('../assets/monster/monster12.png')});
                    break;
                case 25:
                    this.setState({avatar: require('../assets/monster/monster13.png')});
                    break;
                case 26:
                    this.setState({avatar: require('../assets/monster/monster14.png')});
                    break;
                case 27:
                    this.setState({avatar: require('../assets/monster/monster15.png')});
                    break;
                case 28:
                    this.setState({avatar: require('../assets/monster/monster16.png')});
                    break;
                case 29:
                    this.setState({avatar: require('../assets/monster/monster17.png')});
                    break;
                case 30:
                    this.setState({avatar: require('../assets/monster/monster18.png')});
                    break;
                case 31:
                    this.setState({avatar: require('../assets/monster/monster19.png')});
                    break;
                case 32:
                    this.setState({avatar: require('../assets/monster/monster20.png')});
                    break;
                case 33:
                    this.setState({avatar: require('../assets/monster/monster21.png')});
                    break;
                case 34:
                    this.setState({avatar: require('../assets/monster/monster22.png')});
                    break;
                case 35:
                    this.setState({avatar: require('../assets/monster/monster23.png')});
                    break;
                case 36:
                    this.setState({avatar: require('../assets/monster/monster24.png')});
                    break;
                case 37:
                    this.setState({avatar: require('../assets/monster/monster25.png')});
                    break;
                case 38:
                    this.setState({avatar: require('../assets/monster/monster26.png')});
                    break;
                case 39:
                    this.setState({avatar: require('../assets/monster/monster27.png')});
                    break;
                case 40:
                    this.setState({avatar: require('../assets/monster/monster28.png')});
                    break;
                case 41:
                    this.setState({avatar: require('../assets/monster/monster29.png')});
                    break;
                case 42:
                    this.setState({avatar: require('../assets/monster/monster30.png')});
                    break;
                case 43:
                    this.setState({avatar: require('../assets/monster/monster31.png')});
                    break;
                case 44:
                    this.setState({avatar: require('../assets/monster/monster32.png')});
                    break;
                case 45:
                    this.setState({avatar: require('../assets/monster/monster33.png')});
                    break;
                case 46:
                    this.setState({avatar: require('../assets/monster/monster34.png')});
                    break;
                case 47:
                    this.setState({avatar: require('../assets/monster/monster35.png')});
                    break;
                case 48:
                    this.setState({avatar: require('../assets/monster/monster36.png')});
                    break;
                case 49:
                    this.setState({avatar: require('../assets/monster/monster37.png')});
                    break;
                case 50:
                    this.setState({avatar: require('../assets/monster/monster38.png')});
                    break;

                default:
                    this.setState({avatar: require('../assets/monster/monster1.png')});
            }
        })
    }

    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true}/>
                <View style={[styles.bodyWidth,styles.backBlack]}>
                    <Text style={[styles.selected_header, styles.center, {marginTop: 10}]}>CONGRATULATIONS</Text>
                    <View style = {[styles.row, styles.center_element]}>
                        <Image style={[styles.contain, {height:380, width:350}]}
                               source={this.state.avatar}/>
                    </View>
                    <Text style={[styles.center, {color: 'white',fontSize: 25}]}>
                        {this.state.user}
                    </Text>
                </View>
            </View>
        );
    }
}