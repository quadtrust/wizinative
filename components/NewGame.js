import React, { Component } from 'react';
import {
  View,
  Text,
  ToastAndroid,
  AsyncStorage
} from 'react-native';
import styles from './Style';
import Header from './Header';
import Button from 'react-native-button';
import Sockets from './Sockets';
import Sounds from './Sounds';

export default class NewGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: false,
      isActiveFriends: false,
      isActiveRandom: false,
      isClassicActive: false,
      isChallengeActive: false,
      isDirectorateActive: false
    }
  }

  componentDidMount() {
    this.props.connectSocket();
    Sockets.on('challenge', (from) => {
      let username = from[0].name;
      let avatar = from[0].avatar;
      this.props.inviteShow(username, () => {
        Sockets.emit('challenge-reply', { answer: true, to: username });
        this.props.doLoad('challenge', { user2: username, avatar: avatar });
      }, () => {
        Sockets.emit('challenge-reply', { answer: false, to: username });
      });
    });
    AsyncStorage.getItem('user').then((sUser) => {
      let user = JSON.parse(sUser);
      if (user.setting.sound === 1)
        Sounds.playBackground();
    })
  }

  _classic() {
    this.setState({
      mode: false,
      isClassicActive: true,
      isChallengeActive: false
    });
  }
  _challenge() {
    this.setState({ mode: true });
    this.setState({ isChallengeActive: true });
    this.setState({ isClassicActive: false });

  }
  _toggleBackFriends() {
    this.setState({
        isActiveFriends: true,
        isActiveRandom: false,
        isDirectorateActive: false
    });
  }
  _toggleBackRandom() {
    this.setState({
        isActiveRandom: true,
        isActiveFriends: false,
        isDirectorateActive: false
    });
  }

  toggleDirectorate() {
    this.setState({
        isDirectorateActive: !this.state.isDirectorateActive,
        isActiveRandom: false,
        isActiveFriends: false
    });
  }

  _play() {
    if (this.state.isClassicActive) {
      this.props.doLoad('spinner')
    } else if (this.state.isChallengeActive && this.state.isActiveFriends) {
      this.props.doLoad('inviteFriend');
    } else if (this.state.isChallengeActive && this.state.isActiveRandom) {
      this.props.doLoad('challenge', { random: true });
    } else if(this.state.isChallengeActive && this.state.isDirectorateActive) {
      this.props.doLoad('directorates');
    }else {
      ToastAndroid.show('Please choose a category first!', ToastAndroid.SHORT);
    }
  }

  render() {
    let challengeMode = <View />;
    if (this.state.mode) {
      challengeMode = <View>
        <Text style={[styles.center, { fontSize: 22, fontWeight: '600', color: '#000', marginTop: 20 }]}>
          CHOOSE YOUR OPPONENT
          </Text>
        <View style={[styles.row, { justifyContent: 'space-around' }]}>
          <Button
            containerStyle={this.state.isActiveFriends ? styles.activeBackFriends : styles.inActiveBackFriends}
            style={[{ fontSize: 18, color: 'white' }]}
            onPress={() => this._toggleBackFriends()}
          >FRIENDS</Button>
          <Button
            containerStyle={this.state.isActiveRandom ? styles.activeBackRandom : styles.inActiveBackRandom}
            onPress={() => this._toggleBackRandom()}
            style={[{ fontSize: 18, color: 'white' }]}
          >RANDOM</Button>

        </View>
        <View style={{ alignItems: 'center' }}>
          <Button
              containerStyle={this.state.isDirectorateActive ? styles.activeBackFriends : styles.inActiveBackFriends}
              style={[{ fontSize: 18, color: 'white' }]}
              onPress={() => this.toggleDirectorate()}
          >DIRECTORATE</Button>
        </View>
      </View>
    }

    return (
      <View style={[styles.fullWidth]}>
        <Header doLoad={this.props.doLoad} doPop={this.props.doPop} player={true} />
        <View style={[styles.bodyWidth, styles.whiteBack]}>
          <View style={{ backgroundColor: '#F06C50' }}>
            <Text style={[styles.white, styles.challenge_header, styles.center]}>NEW GAME</Text>
          </View>
          <Text style={[styles.center, { fontSize: 22, fontWeight: '600', color: '#000', marginTop: 20 }]}>
            SELECT YOUR GAME MODE
              </Text>
          <View style={[styles.row, { justifyContent: 'space-around' }]}>
            <Button
              containerStyle={[styles.classicActive]}
              onPress={() => this._classic()}
              style={this.state.isClassicActive ? styles.activeClassic : styles.inactiveClassic}
            >CLASSIC</Button>
            <Button
              containerStyle={[styles.challengeActive]}
              onPress={() => this._challenge()}
              style={this.state.isChallengeActive ? styles.activeChallenge : styles.inactiveClassic}
            >CHALLENGE</Button>

          </View>
          {challengeMode}
          <Text style={[styles.center, { color: '#000', marginTop: 10 }]}>Challenge mode allows you to play against several challengers</Text>

          <View style={[styles.row, { justifyContent: 'center', marginTop: 30 }]}>
            <Button
              containerStyle={[{
                marginTop: 20,
                padding: 5,
                width: 230,
                borderRadius: 5,
                backgroundColor: '#1EBBA7',
              }]}
              onPress={() => this._play()}
              style={[{ fontSize: 18, color: 'white' }]}
            >PLAY NOW</Button>
          </View>
        </View>
      </View>
    );
  }
}