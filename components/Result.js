import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import Header from './Header';
import PlayerStats from './PlayerStats';
import API from './API';
import language from './Language';

export default class Result extends Component {
    constructor(props) {
        super(props);
        this.state = {
            maths: '0%',
            arabic: '0%',
            islamic: '0%',
            english: '0%',
            social: '0%',
            science: '0%',
            maths_answered: '0',
            arabic_answered: '0',
            islamic_answered: '0',
            english_answered: '0',
            social_answered: '0',
            science_answered: '0',
            work_subject: ''
        }
    }

    componentDidMount() {

        API.getResult(this.props.doResetTo, (resp) => {
            for (let i = 0; i < resp.result.length; i++) {
                switch (resp.result[i].category_name) {
                    case 'Maths':
                        this.setState({ maths_answered: resp.result[i].questions_answered });
                        let maths_percent = Math.round((resp.result[i].questions_asked ? ((resp.result[i].questions_answered_correctly / resp.result[i].questions_asked) * 100) : 0));
                        this.setState({ maths: maths_percent + '%' });
                        break;

                    case 'Islamic Studies':
                        this.setState({ islamic_answered: resp.result[i].questions_answered });
                        let islamic_percent = Math.round((resp.result[i].questions_asked ? ((resp.result[i].questions_answered_correctly / resp.result[i].questions_asked) * 100) : 0));
                        this.setState({ islamic: islamic_percent + '%' });
                        break;

                    case 'Science':
                        this.setState({ science_answered: resp.result[i].questions_answered });
                        let science_percent = Math.round((resp.result[i].questions_asked ? ((resp.result[i].questions_answered_correctly / resp.result[i].questions_asked) * 100) : 0));
                        this.setState({ science: science_percent + '%' });
                        break;

                    case 'English':
                        this.setState({ english_answered: resp.result[i].questions_answered });
                        let english_percent = Math.round((resp.result[i].questions_asked ? ((resp.result[i].questions_answered_correctly / resp.result[i].questions_asked) * 100) : 0));
                        this.setState({ english: english_percent + '%' });
                        break;

                    case 'Arabic':
                        this.setState({ arabic_answered: resp.result[i].questions_answered });
                        let arabic_percent = Math.round((resp.result[i].questions_asked ? ((resp.result[i].questions_answered_correctly / resp.result[i].questions_asked) * 100) : 0));
                        this.setState({ arabic: arabic_percent + '%' });
                        break;

                    case 'Social Studies':
                        this.setState({ social_answered: resp.result[i].questions_answered });
                        let social_percent = Math.round((resp.result[i].questions_asked ? ((resp.result[i].questions_answered_correctly / resp.result[i].questions_asked) * 100) : 0));
                        this.setState({ social: social_percent + '%' });
                        break;
                }

            }
            setTimeout(() => {
                let minimum_marks = Math.min(parseInt(this.state.maths), parseInt(this.state.arabic), parseInt(this.state.english), parseInt(this.state.islamic), parseInt(this.state.science), parseInt(this.state.social));
                if (minimum_marks == parseInt(this.state.maths)) {
                    this.setState({ work_subject: 'MATHS' })
                } else if (minimum_marks == parseInt(this.state.english)) {
                    this.setState({ work_subject: 'ENGLISH' })
                } else if (minimum_marks == parseInt(this.state.islamic)) {
                    this.setState({ work_subject: 'ISLAMIC' })
                } else if (minimum_marks == parseInt(this.state.social)) {
                    this.setState({ work_subject: 'SOCIAL' })
                } else if (minimum_marks == parseInt(this.state.science)) {
                    this.setState({ work_subject: 'SCIENCE' })
                } else if (minimum_marks == parseInt(this.state.arabic)) {
                    this.setState({ work_subject: 'ARABIC' })
                }
            }, 200);

        });
    }

    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true} />
                <PlayerStats count={this.props.spins} life={this.props.lives} />
                <View style={[styles.bodyWidth]}>
                    <ScrollView>
                        <View style={[styles.headerContainer]}>
                            <Text style={[styles.header]}>{language.progressReport}</Text>
                        </View>
                        <View style={[styles.reportContainer]}>
                            <View style={{ flex: 1, padding: 5 }}>
                                <Text style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontWeight: '800',
                                    textAlign: 'center'
                                }}>{language.maths}</Text>
                                <View style={{flexDirection: 'row'}}>
                                    <Image
                                        style={[styles.characterImage]}
                                        source={require('../assets/images/common/maths-character.png')}
                                    >
                                    </Image>
                                    <Text style={[styles.percentageText]}> {this.state.maths}</Text>
                                </View>
                                <Text style={{ fontSize: 10, color: 'white', fontWeight: '800' }}> {language.questionAnswered} {this.state.maths_answered}</Text>
                            </View>
                            <View style={{ padding: 5, flex: 1, borderLeftWidth: 1, borderColor: '#BABCBC' }}>
                                <Text style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontWeight: '800',
                                    textAlign: 'right',
                                    paddingRight: 30
                                }}>{language.arabic}</Text>
                                <View style={{flexDirection: 'row'}}>
                                    <Image
                                        style={[styles.characterImage]}
                                        source={require('../assets/images/common/arabic-character.png')}
                                    >
                                    </Image>
                                    <Text style={[styles.percentageText]}> {this.state.arabic}</Text>
                                </View>
                                <Text style={{ fontSize: 10, color: 'white', fontWeight: '800' }}> {language.questionAnswered} {this.state.arabic_answered}</Text>
                            </View>

                        </View>
                        <View style={[styles.reportContainer]}>
                            <View style={{ flex: 1, padding: 5 }}>
                                <Text style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontWeight: '800',
                                    textAlign: 'center'
                                }}>{language.islamic}</Text>
                                <View style={{flexDirection: 'row'}}>
                                    <Image
                                        style={[styles.characterImage]}
                                        source={require('../assets/images/common/islamic-character.png')}
                                    >
                                    </Image>

                                    <Text style={[styles.percentageText]}> {this.state.islamic}</Text>
                                </View>
                                <Text style={{ fontSize: 10, color: 'white', fontWeight: '800' }}> {language.questionAnswered} {this.state.islamic_answered}</Text>
                            </View>
                            <View style={{ padding: 5, flex: 1, borderLeftWidth: 1, borderColor: '#BABCBC' }}>
                                <Text style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontWeight: '800',
                                    textAlign: 'center',
                                }}>{language.english}</Text>
                                <View style={{flexDirection: 'row'}}>
                                    <Image
                                        style={[styles.characterImage]}
                                        source={require('../assets/images/common/english-character.png')}
                                    >
                                    </Image>

                                    <Text style={[styles.percentageText]}> {this.state.english}</Text>
                                </View>
                                <Text style={{ fontSize: 10, color: 'white', fontWeight: '800' }}>{language.questionAnswered} {this.state.english_answered}</Text>
                            </View>
                        </View>
                        <View style={[styles.reportContainer]}>
                            <View style={{ flex: 1, padding: 5 }}>
                                <Text style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontWeight: '800',
                                    textAlign: 'center',
                                }}>{language.social}</Text>
                                <View style={{flexDirection: 'row'}}>
                                    <Image
                                        style={[styles.characterImage]}
                                        source={require('../assets/images/common/social-character.png')}
                                    >
                                    </Image>

                                    <Text style={[styles.percentageText]}> {this.state.social}</Text>
                                </View>
                                <Text style={{ fontSize: 10, color: 'white', fontWeight: '800' }}> {language.questionAnswered} {this.state.science_answered}</Text>
                            </View>
                            <View style={{ padding: 5, flex: 1, borderLeftWidth: 1, borderColor: '#BABCBC' }}>
                                <Text style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontWeight: '800',
                                    textAlign: 'right',
                                    paddingRight: 30
                                }}>{language.science}</Text>
                                <View style={{flexDirection: 'row'}}>
                                    <Image
                                        style={[styles.characterImage]}
                                        source={require('../assets/images/common/science-character.png')}
                                    >
                                    </Image>

                                    <Text style={[styles.percentageText]}> {this.state.science}</Text>
                                </View>
                                <Text style={{ fontSize: 10, color: 'white', fontWeight: '800' }}> {language.questionAnswered} {this.state.science_answered}</Text>
                            </View>


                        </View>
                        <View style={[styles.resultContainer]}>
                            <Text style={[styles.result]}>
                                {language.resultOverview}
                            </Text>
                        </View>
                        <View style={[styles.tipContainer]}>
                            <Text style={[styles.tip]}>
                                YOU NEED TO WORK ON {this.state.work_subject}
                            </Text>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginLeft: 10,
                            marginRight: 10,
                            paddingTop: 5,
                            paddingBottom: 10
                        }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => this.props.doLoad('spinner')}>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 10 }}>{language.spinAgain}</Text>
                                    <Image
                                        style={[{
                                            height: 55,
                                            width: 55,
                                            resizeMode: 'contain',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }]}
                                        source={require('../assets/images/common/spin.png')}
                                    >
                                    </Image>
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 10 }}>{language.detailReport}</Text>
                                    <Image
                                        style={[{
                                            height: 55,
                                            width: 55,
                                            resizeMode: 'contain',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginLeft: 20
                                        }]}
                                        source={require('../assets/images/report/detailed.png')}
                                    >
                                    </Image>
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => this.props.doLoad('question')}>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 10 }}>{language.addQuestion}</Text>
                                    <Image
                                        style={[{
                                            height: 55,
                                            width: 55,
                                            resizeMode: 'contain',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginLeft: 20
                                        }]}
                                        source={require('../assets/images/report/question.png')}
                                    >
                                    </Image>
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => this.props.doLoad('invite')}>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 10 }}>{language.inviteFriend}</Text>
                                    <Image
                                        style={[{
                                            height: 55,
                                            width: 55,
                                            resizeMode: 'contain',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginLeft: 20
                                        }]}
                                        source={require('../assets/images/report/invite.png')}
                                    >
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            backgroundColor: 'grey',
                            paddingLeft: 10,
                            paddingRight: 10
                        }}>
                            <Text style={{ fontSize: 15, color: '#fff' }}> Share on facebook</Text><Text
                            style={{ fontSize: 15, color: '#fff' }}>Tweet</Text><Text
                            style={{ fontSize: 15, color: '#fff' }}> Send via mail</Text>
                        </View>
                        <View style={[styles.resultContainer, { height: 10 }]}>

                        </View>

                    </ScrollView>

                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#2B292A'
    },
    bodyWidth: {
        flex: 18
    },
    tipContainer: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#4175BB' },
    headerContainer: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#3FC1FF' },
    resultContainer: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#63B9A8' },
    header: { fontSize: 25, color: '#fff', fontWeight: '800' },
    tip: { fontSize: 19, color: '#fff', fontWeight: '800', paddingTop: 10, paddingBottom: 10 },
    result: { fontSize: 18, color: '#fff', fontWeight: '800' },
    reportContainer: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderColor: '#BABCBC',
        paddingBottom: 5,
        marginRight: 5,
        marginLeft: 5
    },
    characterImage: {
        resizeMode: 'contain',
        height: 60,
        width: undefined,
        flex: 1
    },
    percentageText: {
        fontSize: 40,
        color: '#63B9A8',
        fontWeight: '800',
        flex: 2,
        textAlign: 'center'
    }

});