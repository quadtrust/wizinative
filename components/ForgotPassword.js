import React, {Component} from 'react'
import {
    View,
    TextInput,
    Text,
} from 'react-native'
import styles from './Style'
import Button from 'react-native-button'

export default class ForgetPassword extends Component {
    render() {
        return (
            <View style={[styles.fullWidth,styles.backBlack,{justifyContent:'center'}]}>
                <Text style={[styles.headline, styles.center]}>
                    FORGOT PASSWORD
                </Text>
                <View style={{alignItems:'center',marginTop:20}}>
                   <TextInput
                       ref="password"
                       style={[{
                           height: 35,
                           width:300,
                           backgroundColor: '#EFE9E9',
                           borderColor: '#8F8C8D',
                           borderBottomWidth: 1,
                           padding: 5,
                           color:'black',
                           borderRadius: 5}]}
                       placeholder="Enter your email"
                       placeholderTextColor="black"
                       underlineColorAndroid="transparent"
                       returnKeyType="go"
                   />
               </View>
                <View style={{alignItems:'center'}}>
                    <Button
                        containerStyle={[styles.socialButton,styles.red,  styles.createAccountColor,{width:200,marginTop:20}]}
                        style={[styles.whiteColor, styles.smallFont,{color:'white'}]}
                    >Submit</Button>
                </View>
            </View>
        );
    }
}