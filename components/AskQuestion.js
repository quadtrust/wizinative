import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    AsyncStorage,
    TouchableOpacity,
    TextInput,
    ToastAndroid
} from 'react-native';
import  styles from './Style';
import language from './Language';
import Header from './Header';
import API from './API';

export default class AskQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subject_name: 'ENGLISH',
            question: '',
            choice1: '',
            choice2: '',
            choice3: '',
            choice4: '',
            remark: '',
            answer_remark: '',
            teacher: require('../assets/images/common/monster.png'),
            background: require('../assets/images/islam/islam.png'),
            correct_choice_back: require('../assets/images/common/2.png'),
            editing: true,
            choice_1_back: require('../assets/images/common/1.png'),
            choice_2_back: require('../assets/images/common/1.png'),
            choice_3_back: require('../assets/images/common/1.png'),
            choice_4_back: require('../assets/images/common/1.png'),
            correct_choice: 0,
            ClassId: 0,
            CategoryId: 0
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('user').then((respo) => {
            this.setState({
                ClassId: JSON.parse(respo).user.ClassId
            });
        });

        AsyncStorage.getItem('theme').then((resp) => {
            switch (resp) {
                case 'english':
                    this.setState({
                        background: require('../assets/images/english/english.png'),
                        subject_name: language.english,
                        teacher: require('../assets/images/common/english-character.png'),
                        CategoryId: 1
                    });
                    break;

                case 'maths':
                    this.setState({
                        background: require('../assets/images/maths/maths.png'),
                        subject_name: language.maths,
                        teacher: require('../assets/images/common/maths-character.png'),
                        CategoryId: 2
                    });
                    break;

                case 'islam':
                    this.setState({
                        background: require('../assets/images/islam/islam.png'),
                        subject_name: language.islamic,
                        teacher: require('../assets/images/common/islamic-character.png'),
                        CategoryId: 3
                    });
                    break;

                case 'social':
                    this.setState({
                        background: require('../assets/images/sst/sst.png'),
                        teacher: require('../assets/images/common/social-character.png'),
                        subject_name: language.social,
                        CategoryId: 6
                    });
                    break;

                case 'science':
                    this.setState({
                        background: require('../assets/images/science/science.png'),
                        teacher: require('../assets/images/common/science-character.png'),
                        subject_name: language.science,
                        CategoryId: 5
                    });
                    break;

                default:
                    this.setState({
                        background: require('../assets/images/arabic/arabic.png'),
                        teacher: require('../assets/images/common/arabic-character.png'),
                        subject_name: language.arabic,
                        CategoryId: 4
                    });
            }
        });
    }

    nextStep() {
        if (
            this.state.question.length > 0 &&
            this.state.choice1.length > 0 &&
            this.state.choice2.length > 0 &&
            this.state.choice3.length > 0 &&
            this.state.choice4.length > 0 &&
            this.state.editing
        ) {
            this.setState({
                editing: !this.state.editing
            });
            ToastAndroid.show('Select correct choice', ToastAndroid.SHORT);
        } else if (!this.state.editing) {
            if ([1, 2, 3, 4].indexOf(this.state.correct_choice) >= 0) {
                this.props.loading();
                API.suggestQuestion(this.state, this.props.doResetTo, (response) => {
                    this.props.loaded();
                    if (response.error) {
                        ToastAndroid.show('There was some error, please try again.', ToastAndroid.SHORT);
                    } else if (response.success) {
                        ToastAndroid.show('Question was submitted to admin', ToastAndroid.SHORT);
                        this.props.doPop();
                    } else {
                        ToastAndroid.show('There was some error, please try again.', ToastAndroid.SHORT);
                    }
                });
            } else {
                ToastAndroid.show('Select an answer first', ToastAndroid.SHORT);
            }
        } else {
            ToastAndroid.show('Fill all the values', ToastAndroid.SHORT);
        }
    }

    chooseAnswer(number) {
        if (!this.state.editing) {
            if (number === 1) {
                this.setState({
                    choice_1_back: this.state.correct_choice_back,
                    choice_2_back: require('../assets/images/common/1.png'),
                    choice_3_back: require('../assets/images/common/1.png'),
                    choice_4_back: require('../assets/images/common/1.png'),
                    correct_choice: 1
                });
            } else if (number === 2) {
                this.setState({
                    choice_1_back: require('../assets/images/common/1.png'),
                    choice_2_back: this.state.correct_choice_back,
                    choice_3_back: require('../assets/images/common/1.png'),
                    choice_4_back: require('../assets/images/common/1.png'),
                    correct_choice: 2
                });
            } else if (number === 3) {
                this.setState({
                    choice_1_back: require('../assets/images/common/1.png'),
                    choice_2_back: require('../assets/images/common/1.png'),
                    choice_3_back: this.state.correct_choice_back,
                    choice_4_back: require('../assets/images/common/1.png'),
                    correct_choice: 3
                });
            } else if (number === 4) {
                this.setState({
                    choice_1_back: require('../assets/images/common/1.png'),
                    choice_2_back: require('../assets/images/common/1.png'),
                    choice_3_back: require('../assets/images/common/1.png'),
                    choice_4_back: this.state.correct_choice_back,
                    correct_choice: 4
                });
            } else {
                ToastAndroid.show('Select correct choice', ToastAndroid.SHORT);
            }
        }
    }

    render() {
        return (
            <View style={[styles.fullWidth, {opacity: this.state.fade}]} accessible={false}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true}/>

                <View style={[styles.bodyWidth]}>
                    <Image
                        style={[styles.fullWidth, styles.zoomed, styles.notKnown]}
                        source={this.state.background}
                    >
                        <Image
                            style={[styles.blackBoard]}
                            source={require('../assets/images/common/board.png')}
                        >
                            <Text style={styles.headerText}> {this.state.subject_name} </Text>
                            { this.state.editing ? <TextInput
                                style={{
                                    flex: 1,
                                    margin: 15,
                                    textAlign: 'center',
                                    justifyContent: 'center',
                                    color: 'white'
                                }}
                                placeholder={'Write your question here'}
                                placeholderTextColor={'white'}
                                onChangeText={question => this.setState({question})}
                                value={this.state.question}
                                multiline={true}
                            /> : <Text style={{
                                color: 'white',
                                fontSize: 18,
                                textAlign: 'center',
                                margin: 15
                            }}>{this.state.question}</Text> }
                        </Image>
                        <TouchableOpacity style={styles.flexContainer}
                                          onPress={() => this.chooseAnswer(1)}
                        >
                            <Image
                                style={styles.choiceBackground}
                                source={this.state.choice_1_back}
                            >
                                { this.state.editing ? <TextInput
                                    style={{flex: 1, textAlign: 'center', fontWeight: 'bold', fontSize: 16}}
                                    placeholder={'Choice 1'}
                                    onChangeText={choice1 => this.setState({choice1})}
                                    value={this.state.choice1}
                                /> : <Text style={styles.choice}> {this.state.choice1} </Text> }

                            </Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.flexContainer}
                                          onPress={() => this.chooseAnswer(2)}
                        >
                            <Image
                                style={styles.choiceBackground}
                                source={this.state.choice_2_back}
                            >
                                { this.state.editing ? <TextInput
                                    style={{flex: 1, textAlign: 'center', fontWeight: 'bold', fontSize: 16}}
                                    placeholder={'Choice 2'}
                                    onChangeText={choice2 => this.setState({choice2})}
                                    value={this.state.choice2}
                                /> : <Text style={styles.choice}> {this.state.choice2} </Text> }
                            </Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.flexContainer}
                                          onPress={() => this.chooseAnswer(3)}
                        >
                            <Image
                                style={styles.choiceBackground}
                                source={this.state.choice_3_back}
                            >
                                { this.state.editing ? <TextInput
                                    style={{flex: 1, textAlign: 'center', fontWeight: 'bold', fontSize: 16}}
                                    placeholder={'Choice 3'}
                                    onChangeText={choice3 => this.setState({choice3})}
                                    value={this.state.choice3}
                                /> : <Text style={styles.choice}> {this.state.choice3} </Text> }
                            </Image>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.flexContainer}
                                          onPress={() => this.chooseAnswer(4)}
                        >
                            <Image
                                style={styles.choiceBackground}
                                source={this.state.choice_4_back}
                            >
                                { this.state.editing ? <TextInput
                                    style={{flex: 1, textAlign: 'center', fontWeight: 'bold', fontSize: 16}}
                                    placeholder={'Choice 4'}
                                    onChangeText={choice4 => this.setState({choice4})}
                                    value={this.state.choice4}
                                /> : <Text style={styles.choice}> {this.state.choice4} </Text> }
                            </Image>
                        </TouchableOpacity>
                        <View style={{position: 'absolute', left: 10}}>
                            <Image source={this.state.teacher}
                                   style={{
                                       resizeMode: 'contain',
                                       height: 120,
                                       width: 130,
                                       zIndex: 9999
                                   }}
                            />
                        </View>
                        <View style={{
                            backgroundColor: 'rgba(0,0,0,0.65)',
                            flexDirection: 'row-reverse',
                            marginTop: 60,
                            padding: 10
                        }}>
                            <TouchableOpacity style={{flexDirection: 'row'}} onPress={() => this.nextStep()}>
                                <Text style={[styles.nextText]}>NEXT STEP</Text>
                                <Image style={{height: 30, width: 30, resizeMode: 'contain'}}
                                       source={require('../assets/images/common/next.png')}/>
                            </TouchableOpacity>
                        </View>
                    </Image>
                </View>
            </View>
        );
    }
}