import React, {Component} from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    ToastAndroid,
    AsyncStorage
} from 'react-native'
import Header from  './Header'
import API from './API'
import language from './Language'
let id = '';

export default class Selected extends Component {

    constructor(props) {
        super(props);
        this.state = {
            subject: '',
            image: require('../assets/images/common/arabic-character.png'),
            isBigText: false
        }
    }


    componentDidMount() {

        AsyncStorage.getItem('subject').then((resp) => {
            if (resp == 'MATHS') {
                this.setState({subject: language.maths});
                this.setState({image: require('../assets/images/common/maths-character.png')})
            } else if (resp == 'SCIENCE') {
                this.setState({subject: language.science});
                this.setState({image: require('../assets/images/common/science-character.png')})
            } else if (resp == 'ARABIC') {
                this.setState({subject: language.arabic});
                this.setState({image: require('../assets/images/common/arabic-character.png')})
            } else if (resp == 'ISLAMIC STUDIES') {
                this.setState({subject: language.arabic});
                this.setState({isBigText: true});
                this.setState({image: require('../assets/images/common/islamic-character.png')})
            } else if (resp == 'ENGLISH') {
                this.setState({subject: language.english});
                this.setState({image: require('../assets/images/common/english-character.png')})
            } else if (resp == 'SOCIAL STUDIES') {
                this.setState({subject: language.social});
                this.setState({isBigText: true});
                this.setState({image: require('../assets/images/common/social-character.png')})
            } else {
                this.setState({image: require('../assets/images/common/social-character.png')})
            }


        });
    }

    _takeTest() {
        switch (this.state.subject) {
            case language.maths:
                id = 2;
                AsyncStorage.setItem('theme', 'maths');
                break;
            case language.science:
                id = 5;
                AsyncStorage.setItem('theme', 'science');
                break;
            case language.arabic:
                id = 4;
                AsyncStorage.setItem('theme', 'arabic');
                break;
            case language.islamic:
                id = 3;
                AsyncStorage.setItem('theme', 'islam');
                break;
            case language.english:
                id = 1;
                AsyncStorage.setItem('theme', 'english');
                break;
            case language.social:
                id = 6;
                AsyncStorage.setItem('theme', 'social');
                break;
        }
        if (id == '') {
            ToastAndroid.show('ERROR!', ToastAndroid.SHORT);
        } else {
            API.start_game(id, this.props.doResetTo, (resp) => {
                if (resp.error) {
                    ToastAndroid.show('Please spin again!', ToastAndroid.SHORT);
                } else {
                    AsyncStorage.setItem('question', JSON.stringify(resp)).then(() => {
                        this.props.doResetTo('subject');
                    });

                }
            });
        }
    }


    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true}/>
                <View style={[styles.bodyWidth]}>
                    <View style={{flex: 0.7}}>
                        <Text
                            style={[this.state.isBigText ? styles.bigFont : styles.heading, styles.blue, styles.textCenter]}>
                            {this.state.subject}
                        </Text>
                    </View>
                    <View style={{flex: 0.3}}>
                        <Text style={[styles.bold, styles.black, styles.textCenter, styles.mediumFont]}>
                            {language.selected}
                        </Text>
                    </View>
                    <View style={[ {flex: 2}]}>
                        <Image style={[styles.contain, {height: null, width: null, flex:2}]} source={this.state.image}/>

                        <Text style={[styles.textCenter, styles.orange, styles.bold, styles.h1]}>
                            {language.option}
                        </Text>
                    </View>
                    <View style={{flex: 0.8, flexDirection: 'row'}}>

                        <TouchableOpacity style={[{
                            flex: 1, borderRightWidth: 1,
                            borderRightColor: '#555253',
                            marginTop: 30,
                            marginBottom: 10
                        }]} onPress={() => this._takeTest()}>
                            <Image source={require('../assets/images/spin_selection/test.png')}
                                   style={{height: null, width: null, resizeMode: 'contain', flex: 1}}
                            />
                            <Text style={{
                                flex: 1,
                                color: 'black',
                                textAlign: 'center',
                                fontWeight: 'bold'
                            }}>{language.takeTest}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[{
                            flex: 1, borderRightWidth: 1,
                            borderRightColor: '#555253',
                            marginTop: 30,
                            marginBottom: 10
                        }]} onPress={() => {
                            this.props.doPop()
                        }}>
                            <Image source={require('../assets/images/spin_selection/respin.png')}
                                   style={{height: null, width: null, resizeMode: 'contain', flex: 1}}
                            />
                            <Text style={{flex: 1, color: 'black', textAlign: 'center', fontWeight: 'bold'}}>
                                {language.spinAgain}
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[{flex: 1, marginTop: 30, marginBottom: 10}]}
                                          onPress={() => {
                                              this.props.doLoad('subjectSelection')
                                          }}>
                            <Image source={require('../assets/images/spin_selection/subject.png')}
                                   style={{height: null, width: null, resizeMode: 'contain', flex: 1}}
                            />
                            <Text style={{flex: 1, color: 'black', textAlign: 'center', fontWeight: 'bold'}}>
                                {language.subjectSelection}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: 'white'
    },
    heading: {
        fontSize: 75,
        fontWeight: '800',
    },
    bigFont: {
        fontSize: 45,
        fontWeight: '800'
    },
    h1: {
        fontSize: 25
    },
    black: {
        color: 'black'
    },
    mediumFont: {
        fontSize: 20
    },
    orange: {
        color: '#FF9D00'
    },
    blue: {
        color: '#4B82C3'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCenter: {
        textAlign: 'center'
    },
    bodyWidth: {
        flex: 18
    },
    bold: {
        fontWeight: '800'
    },

    zoomed: {
        resizeMode: 'cover'
    },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
});