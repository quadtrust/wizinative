import React, {Component, PropTypes} from 'react';
import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Text,
    AsyncStorage,
    ToastAndroid
} from 'react-native';

export default class Header extends Component {
    static propTypes = {
        doLoad: PropTypes.func.isRequired,
        doPop: PropTypes.func.isRequired
    };
    constructor(props){
        super(props);
        this.state ={
            username:''
        }
    }
    componentDidMount(){
        AsyncStorage.getItem('user').then((resp) =>{
            let user = JSON.parse(resp);
            this.setState({username:user.user.username})
        })
    }

    render() {

        let endGame = <Text style={{flex: 2}} />;
        if (this.props.endGame) {
            endGame = <TouchableOpacity style={{flex: 2, marginTop: 20}} onPress={() => {this.props.end_game()}}>
                <Text style={{color: 'white', fontWeight: '700'}}>End Game</Text>
            </TouchableOpacity>;
        } else if(this.props.back){
            endGame = <TouchableOpacity style={{flex: 1, marginTop: 20, marginRight:12}} onPress={() => this.props.doPop()}>
                <Text style={{color: 'white', fontWeight: '700'}}>BACK</Text>
            </TouchableOpacity>;
        } else if(this.props.player){
            endGame = <View style={{marginRight:5,marginTop:10}}>
                <View >
                    <Text style={{color:'white',textAlign:'center',fontWeight:'700'}}>Player</Text>
                </View>
                <View style={{borderTopWidth:2,borderColor:'#565657'}}>
                    <Text style={{color:'#F1FF0F',textAlign:'center',fontWeight:'700'}}>{this.state.username}</Text>
                </View>
            </View>
        }
        return (
            <View style={[styles.fullWidth, styles.row, styles.height, styles.blackColor]}>
                <Image
                    style={[styles.unknown, styles.fullWidth, styles.positionChoice, {margin: 15}]}
                    source={require('../assets/images/name-nospace.png')}
                />
                <TouchableOpacity onPress={() => this.props.doLoad('menu')}
                                  style={[styles.fullWidth, styles.leftMargin]}>
                    <Image
                        style={[styles.unknown, styles.fullWidth]}
                        source={require('../assets/images/menu-button.png')}/>
                </TouchableOpacity>
                <View style={{flex: 7}}></View>
                {endGame}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1
    },
    row: {
        flexDirection: 'row'
    },
    height: {
        height: 30
    },
    blackColor: {
        backgroundColor: '#403c3d'
    },
    unknown: {
        height: undefined,
        width: undefined,
        resizeMode: 'contain'
    },
    leftMargin: {
        marginLeft: 15
    },
    positionChoice: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    }
});