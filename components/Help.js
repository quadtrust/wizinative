import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native'
import Header from './Header'
import language from './Language'

export default class Help extends Component {

    render() {
        return (
            <View style={styles.fullWidth}>
                <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true} />
                <View style={styles.bodyWidth}>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={[{ fontSize: 30, color: 'skyblue', fontWeight: '800', marginTop: 25 }]}>
                            {language.help}
                        </Text>
                    </View>
                    <View style={{ borderBottomWidth: 2, borderColor: '#D1D2D2', paddingBottom: 25, marginTop: 35 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#000', fontSize: 20, fontWeight: '800', paddingLeft: 20 }}>
                                {language.learn}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ borderBottomWidth: 2, borderColor: '#D1D2D2', paddingBottom: 25, paddingTop: 25 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#000', fontSize: 20, fontWeight: '800', paddingLeft: 20 }}>
                                {language.rule}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ borderBottomWidth: 2, borderColor: '#D1D2D2', paddingBottom: 25, paddingTop: 25 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#000', fontSize: 20, fontWeight: '800', paddingLeft: 20 }}>
                                {language.shop}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ borderBottomWidth: 2, borderColor: '#D1D2D2', paddingBottom: 25, paddingTop: 25 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#000', fontSize: 20, fontWeight: '800', paddingLeft: 20 }}>
                                {language.termsUse}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ borderBottomWidth: 2, borderColor: '#D1D2D2', paddingBottom: 25, paddingTop: 25 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#000', fontSize: 20, fontWeight: '800', paddingLeft: 20 }}>
                                {language.privacy}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1
    },

    bodyWidth: {
        flex: 18,
        backgroundColor: '#fff'
    }

});