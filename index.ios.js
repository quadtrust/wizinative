/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import Loader from './components/Loader'
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class wizi extends Component {
    render() {
        return (
            <View style={styles.fullWidth}>
                <Loader />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1
    }
});

AppRegistry.registerComponent('wizi', () => wizi);
